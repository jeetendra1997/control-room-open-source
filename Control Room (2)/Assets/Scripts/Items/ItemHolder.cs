﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHolder : Interactable {

    public Item item;
    public Transform itemPosition;
    public bool disable;

    private void Start()
    {
        if (item)
            DepositItem(item);
    }

    private void Update()
    {
        DropOnPickUp();
    }

    public void DropOnPickUp()
    {
        if (item)
        {
            if (item.holder)
            {                
                item = null;
            }
        }
    }

    public void SetDisable(bool state) {
        disable = state;
        if (item)
            item.GetComponent<Collider>().enabled = !disable;

    }

    [PunRPC]
    public override void RPCInteract(int characterId) {
        if (disable) return;
        Character character = Manager.Instance.GetBehaviour<Character>(characterId);        
        if (item)
        {
            if (!character.heldItem)
            {
                Item oldItem = item;
                DropItem();
                character.PickUp(oldItem);
            }
        }
        else if (character.heldItem) {
            if (character.heldItem.GetComponent<Beaker>()) {
                item = character.heldItem;
                character.Drop(character.heldItem, false);                
                DepositItem(item);
            }
        }
    }

    void DepositItem(Item item) {
        //if (PhotonNetwork.inRoom)
            //photonView.RPC("RPCDepositItem", PhotonTargets.AllBuffered, item.id);
        //else
            RPCDepositItem(item.id);
    }

    void DropItem() {
        //if (PhotonNetwork.inRoom)
            //photonView.RPC("RPCDropItem", PhotonTargets.AllBuffered);
        //else
            RPCDropItem();
    }

    [PunRPC]
    public void RPCDepositItem(int id) {
        Item newItem = Manager.Instance.GetBehaviour<Item>(id);
        if (!newItem) return;
        item = newItem;
        item.transform.position = itemPosition.position;
        item.transform.rotation = itemPosition.rotation;
        item.transform.SetParent(itemPosition);
        //item.GetComponent<Rigidbody>().isKinematic = true;
        Manager.Instance.SetKinematic(item.gameObject);
    }    

    [PunRPC]
    public void RPCDropItem() {        
        item.transform.SetParent(null);
        item.GetComponent<Rigidbody>().isKinematic = false;
        Debug.Log("unkinematic");
        item = null;
    }
}
