﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MultiplayerBehaviour {    

	public void Interact(Character character)
    {
        if (PhotonNetwork.inRoom)
            photonView.RPC("RPCInteract", PhotonTargets.AllBuffered, character.id);
        else
            RPCInteract(character.id);
    }

    [PunRPC]
    public virtual void RPCInteract(int characterId)
    {

    }
}
