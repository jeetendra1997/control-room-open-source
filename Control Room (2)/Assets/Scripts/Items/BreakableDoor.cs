﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableDoor : Character
{
    public BlastDoor door;
    public GameObject destroyParticle;
    public AudioClip destroyClip;
    Vector3 currentOffset = Vector3.zero;

    private void Awake()
    {
        RegisterID();
        door = GetComponentInParent<BlastDoor>();
        door.doors.Add(this);
        foreach (Limb limb in GetComponentsInChildren<Limb>())
        {
            limbs.Add(limb);
            limb.SetCharacter(this);
        }        
    }

    [PunRPC]
    public override void RPCDamage(int attackerId, float damage, Vector3 direction, int limbId)
    {        
        DamageInfo info = DamageInfo.GetInfo(this, attackerId, damage, direction, limbId);
        door.health -= info.damage;
        if (door.health <= 0)
            Die(info);
        else        
            StartCoroutine(Shake());        
    }
    

    IEnumerator Shake()
    {
        if (currentOffset == Vector3.zero)
        {
            currentOffset = new Vector3(Random.Range(-0.02f, 0.02f), Random.Range(-0.02f, 0.02f), Random.Range(-0.02f, 0.02f));
            door.transform.position += currentOffset;            
            yield return null;
            door.transform.position -= currentOffset;
            currentOffset = Vector3.zero;
        }
    }

    [PunRPC]
    public override void RPCDie(int characterId, float damage, Vector3 direction, int limbId)
    {
        if (dead) return;
        dead = true;
        //Instantiate(destroyParticle, transform.position, transform.rotation);
        Manager.CreateParticle(ParticleType.DoorBreak, transform.position, transform.rotation);
        Manager.CreateAudio(transform.position, destroyClip);
        door.UIButton.GetComponent<Renderer>().material.color = Color.grey;
        foreach (PF_Node node in door.nodes)
        {
            node.blocked = false;
        }
        foreach (PF_Node node in door.nodes)
        {
            node.grid.AssignWeight();
        }
        door.Break();
    }


}
