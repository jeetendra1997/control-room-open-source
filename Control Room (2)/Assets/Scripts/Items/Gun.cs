﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Ammo
{
    public GunType type;
    public int amount;    
}

public enum GunType
{
    Pistol,
    Shotgun,
    MachineGun,    
    Extractor,
}

[System.Serializable]
public class GunDef
{
    public GunType type;

    //public int ammoLimit;
    public int clipSize;
    public int maxAmmo;
    public int projectileCount = 1;
    public float damage;
    public float spread;
    public float knockBack = 10;
    public Vector3 muzzleFlashSize;
}

public class Gun : Weapon {

    public GunType type;
    [HideInInspector]
    public GunDef def;

    public LineRenderer lineRenderer;
    public AudioClip noAmmoClip;

    public Transform projectilePosition;
            
    public DestroyDelay muzzleFlash;
        
    public int ammo;    
    public bool infiniteAmmo;

    public bool isFull { get { return ammo >= def.clipSize; } }
    public bool isEmpty { get { return ammo <= 0; } }

    public class HitPoint {
        public Vector3 position;
        public int particleId = -1;
    }

    List<HitPoint> hitPoints = new List<HitPoint>();

    public float lineRenderTime;    

    private void Start() {
        Initialize();

        if (projectilePosition)
        {
            muzzleFlash = Instantiate(GlobalManager.GameDef.muzzleFlashPrefab, projectilePosition.transform.position, projectilePosition.transform.rotation,projectilePosition).GetComponent<DestroyDelay>();            
            muzzleFlash.gameObject.SetActive(false);
        }

        audioSource = GetComponent<AudioSource>();
        if (holder != null) {
            Character preHolder = holder;
            holder = null;
            Interact(preHolder);
        }

        if (def.projectileCount == 0) def.projectileCount = 1;
        lineRenderer = GetComponent<LineRenderer>();
        if (lineRenderer) {
            lineRenderer.enabled = false;
            lineRenderer.positionCount = def.projectileCount * 2;
        }        
    }

    public virtual void Initialize()
    {
        GunDef def = GlobalManager.GameDef.GetGunDef(type);
        this.def = def;
        ammo = def.clipSize;
        damageInfo.damage = def.damage;
        if (projectilePosition)
            projectilePosition.transform.localScale = def.muzzleFlashSize;
    }

    private void Update()
    {
        if (lineRenderer && lineRenderer.enabled)
        {
            lineRenderTime -= Time.deltaTime;
            if (lineRenderTime <= 0)
            {
                lineRenderer.enabled = false;
                //if (muzzleFlash.gameObject.activeSelf)
                    //muzzleFlash.gameObject.SetActive(false);
            }
        }
    }
    List<Vector3> positions = new List<Vector3>();
    List<int> ids = new List<int>();
    [PunRPC]
    public override void RPCUse() {
        if (ammo > 0) {
            hitPoints.Clear();
            damageInfo.attacker = holder;            

            if (holder.photonView.isMine || !PhotonNetwork.inRoom) {                
                for (int i = 0; i < def.projectileCount; i++) {                    
                    HitPoint newPoint = new HitPoint();
                    damageInfo.direction = projectilePosition.forward;
                    RaycastHit hit;
                    if (Physics.Raycast(projectilePosition.position - (projectilePosition.forward) / 2, projectilePosition.forward + Spread(), out hit, 50, damageLayerMask)) {
                        Damagable damagable = hit.collider.GetComponent<Damagable>();
                        bool canDamage = true;
                        if (damagable) {
                            canDamage = (CanDamage(damagable.damageMasks));
                            if (canDamage) {
                                if (!PhotonNetwork.inRoom || (holder.GetComponent<Player>() && holder.photonView.isMine) || (!holder.GetComponent<Player>()))
                                    damagable.Damage(damageInfo);
                            }
                        }
                        if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Environment") || !damagable) {
                            newPoint.particleId = 0;
                        } else if (canDamage) {
                            newPoint.particleId = 1;
                        }
                    }
                    if (lineRenderer) {
                        Vector3 hitPos = Vector3.zero;
                        if (!hit.collider) {
                            hitPos = projectilePosition.position + projectilePosition.forward * 50;
                            newPoint.position = hitPos;
                        } else {
                            hitPos = hit.point;
                            newPoint.position = hitPos;
                        }
                    }
                    hitPoints.Add(newPoint);
                }

                positions.Clear();
                ids.Clear();
                for (int i = 0; i < hitPoints.Count; i++) {
                    positions.Add(hitPoints[i].position);
                    ids.Add(hitPoints[i].particleId);                    
                }
                if (PhotonNetwork.inRoom)
                    photonView.RPC("RPCFirePoints", PhotonTargets.AllBuffered, positions.ToArray(), ids.ToArray());
                else
                    RPCFirePoints(positions.ToArray(), ids.ToArray());

                if (holder.isPlayer)
                {
                    transform.localEulerAngles += Vector3.left * def.knockBack;
                    (holder as Player).cameraEuler.x -= def.knockBack/6;
                }
            }
            if (!infiniteAmmo) {
                ammo--;
            }
        } else {
            Manager.PlayAudio(audioSource, noAmmoClip);
        }
        SetAttackTime();
    }

    [PunRPC]
    void RPCFirePoints(Vector3[] positions, int[] ids) {

        hitPoints.Clear();
        for(int i = 0; i < positions.Length; i++) {
            hitPoints.Add(new HitPoint() { position = positions[i], particleId = ids[i] });
        }
        AudioSource aSource = Manager.CreateAudio(transform.position, fireClip, new Vector2(0.8f, 1.2f), 1, 30);
        if (aSource)
            aSource.SetCustomCurve(AudioSourceCurveType.CustomRolloff, audioSource.GetCustomCurve(AudioSourceCurveType.CustomRolloff));

        if (muzzleFlash)
        {            
            muzzleFlash.Play();
        }

        for (int i = 0; i < hitPoints.Count; i++) {

            Vector3 forward = (hitPoints[i].position - projectilePosition.position);
            damageInfo.direction = forward;

            if (hitPoints[i].particleId == 0) {
                Manager.CreateAudio(hitPoints[i].position, hitMetalClip);
                Manager.CreateParticle(ParticleType.Metal, hitPoints[i].position, Quaternion.LookRotation(-forward));
                //Instantiate(metalParticle, hitPoints[i].position, Quaternion.LookRotation(-forward));
            } else if (hitPoints[i].particleId == 1) {
                Manager.CreateAudio(hitPoints[i].position, hitFleshClip);
                Manager.CreateParticle(ParticleType.Flesh, hitPoints[i].position, Quaternion.LookRotation(-forward));
                //Instantiate(fleshParticle, hitPoints[i].position, Quaternion.LookRotation(-forward));
            }
            
            if (lineRenderer) {
                lineRenderTime = 1;
                lineRenderer.enabled = true;
                Vector3 hitPos = hitPoints[i].position;                
                lineRenderer.SetPosition(i * 2, projectilePosition.position);
                lineRenderer.SetPosition((i * 2) + 1, hitPos);
            }        
        }
        hitPoints.Clear();
        StartCoroutine(ResetLine());        
    }

    public IEnumerator ResetLine()
    {
        yield return null;
        lineRenderer.enabled = false;        
    }

    public void Reload(int ammo)
    {
        if (PhotonNetwork.inRoom)
            photonView.RPC("RPCReload", PhotonTargets.AllBuffered, ammo);
        else
            RPCReload(ammo);
    }

    [PunRPC]
    void RPCReload(int ammo)
    {
        this.ammo += ammo;
    }

    public Vector3 Spread()
    {
        return projectilePosition.rotation * new Vector3(Random.Range(-def.spread, def.spread), Random.Range(-def.spread, def.spread), Random.Range(-def.spread, def.spread));
    }
}
