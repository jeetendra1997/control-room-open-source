﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvacuationDoor : ScreenButton {

    public AudioSource audioSource;
    public AudioClip rejectClip;
    public AudioClip acceptClip;

    private void Update()
    {
        if (Manager.debugMode)
        {
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                MultiplayerManager.rescueArrived = true;
                Interact(Player.Instance);
            }
        }
    }

    [PunRPC]
    public override void RPCInteract(int characterId) {
        
        if (MultiplayerManager.rescueArrived) {
            Manager.PlayAudio(audioSource, acceptClip);
            base.RPCInteract(characterId);

            Player player = Manager.Instance.GetBehaviour<Player>(characterId);
            bool hasCure = player.inventory.HasItem(MultiplayerManager.cure);

            MultiplayerManager.Instance.RemovePlayer(player, true, hasCure);
        } else {
            Manager.PlayAudio(audioSource, rejectClip);
        }
    }
}
