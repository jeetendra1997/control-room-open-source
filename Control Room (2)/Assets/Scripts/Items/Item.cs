﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType {
    Unarmed,
    OneHand,
    TwoHand,
}

public class Item : Interactable {

    public ItemType itemType;
    public Sprite image;
    public Character holder;


    public virtual void OnPickUp()
    {

    }

    [PunRPC]
    public override void RPCInteract(int characterId)
    {
        Character character = Manager.Instance.GetBehaviour<Character>(characterId);
        if (!holder)
        character.PickUp(this);
        OnPickUp();
    }

    public void SetHolder(Character character)
    {
        holder = character;
        //GetComponent<Rigidbody>().isKinematic = true;
        Manager.Instance.SetKinematic(gameObject);
        GetComponent<Collider>().enabled = false;
    }

    public void Drop(bool RPC = true)
    {
        if (PhotonNetwork.inRoom && RPC)
            photonView.RPC("RPCDrop", PhotonTargets.AllBuffered);
        else
            RPCDrop();
    }

    [PunRPC]
    public void RPCDrop()
    {
        holder = null;
        transform.parent = null;
        GetComponent<Rigidbody>().isKinematic = false;
        Debug.Log("unkinematic");
        GetComponent<Collider>().enabled = true;
    }

    public void Use()
    {
        if (PhotonNetwork.inRoom)
            photonView.RPC("RPCUse", PhotonTargets.AllBuffered);
        else
            RPCUse();
    }

    [PunRPC]
    public virtual void RPCUse() {

    }

    public virtual bool CanUse()
    {
        return true;
    }
}
