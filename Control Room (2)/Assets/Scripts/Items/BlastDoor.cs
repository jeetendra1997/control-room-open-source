﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlastDoor : MultiplayerBehaviour {

    public bool disable;
    public bool destroyed;
    public float health;
    public Animator animator;    
    public bool open;
    public bool opening;
    public float openTime = 2;
    public List<PF_Node> nodes = new List<PF_Node>();

    public GameObject UIPrefab;
    public WorldButton UIButton;
    public Color openColor;
    public Color closedColor;

    public List<ScreenButton> screenButtons = new List<ScreenButton>();
    public List<BreakableDoor> doors = new List<BreakableDoor>();

   
    private void Start()
    {
        if (disable) return;
        animator = GetComponent<Animator>();
        UIButton = Instantiate(UIPrefab, transform.position + (Vector3.up * 20) + (transform.forward * 3), transform.rotation, Manager.UIContainer).GetComponent<WorldButton>();
        UIButton.Event.AddListener(delegate { CRInterface.Instance.InteractWithDoor(this);});

        animator.SetBool("open", open);
        SetState(open);
        SetColor();
    }

    public void AddButton(ScreenButton button)
    {
        screenButtons.Add(button);
        button.Event.AddListener(delegate { Interact(); });
    }
      

    public void Interact()
    {
        if (PhotonNetwork.inRoom)
            photonView.RPC("RPCInteract", PhotonTargets.AllBufferedViaServer);
        else
            RPCInteract();
    }

    [PunRPC]
    public void RPCInteract()
    {
        if (destroyed) return;
        if (opening) return;
        opening = true;
        open = !open;
        OpenDoor(open);
    }

    public void OpenDoor(bool state)
    {        
        open = state;
        animator.SetBool("open", open);
        SetColor();
        GetComponent<AudioSource>().Play();        
    }

    void SetColor()
    {
        if (disable) return;
        if (open)
        {
            UIButton.GetComponent<Renderer>().material.color = openColor;
            UIButton.initialColor = openColor;
            foreach(ScreenButton button in screenButtons)            
                button.SetColor(button.onColor);            
        }
        else
        {
            UIButton.GetComponent<Renderer>().material.color = closedColor;
            UIButton.initialColor = closedColor;
            foreach (ScreenButton button in screenButtons)
                button.SetColor(button.offColor);
        }
    }
    
    public void Break()
    {
        destroyed = true;
        for (int i = 0; i < transform.childCount; i++)
        {
            if (!transform.GetChild(i).gameObject.name.Contains("Interface"))
                Destroy(transform.GetChild(i).gameObject);
        }
    }

    public IEnumerator SetState(bool state)
    {
        yield return new WaitForSeconds(openTime);
        opening = false;
        //open = state;        
        foreach (PF_Node node in nodes)
        {
            node.blocked = !state;
        }
        foreach(PF_Node node in nodes)
        {
            node.grid.AssignWeight();
        }
    }
}
