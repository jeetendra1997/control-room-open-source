﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraInterface : MultiplayerBehaviour
{
    public bool disable;    
    new public Camera camera;
    public Transform camPosition;

    public int frameDelay;
    int currentFrame;

    public GameObject UIPrefab;
    public GameObject UIButton;    

    private void Start()
    {
        if (frameDelay == 10 || frameDelay == 20) frameDelay = 5;
        currentFrame += Random.Range(0, frameDelay);
        if (disable) return;
        Vector3 rotEuler = camPosition.transform.eulerAngles;
        rotEuler.y -= 90; rotEuler.x = 0; rotEuler.z = 0;
        Quaternion rotation = Quaternion.Euler(rotEuler);
        if (!UIPrefab) return;
        UIButton = Instantiate(UIPrefab, transform.position + (Vector3.up * 20) + (-(rotation * Vector3.right)), rotation, Manager.UIContainer);
        UIButton.GetComponent<WorldButton>().Event.AddListener(delegate { CRInterface.Instance.SelectCamera(this); });
    }

    void Update()
    {
        if (camera != null)
        FrameRate();
    }

    void FrameRate()
    {
        currentFrame++;
        if (currentFrame >= frameDelay)
        {
            currentFrame = 0;
            camera.enabled = true;
        }
        else
            camera.enabled = false;
    }

}