﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngredientProcessor : ItemHolder {

    public GameObject activeButton;
    public TextMesh tempText;
    public TextMesh setTempText;
    public float tempLimit = 30;
    public Ingredient.Temperature temperature;    
    public bool complete;
    public ParticleSystem[] particleSystems;
    Beaker beaker;
    public ParticleSystem activeParticleSystem;

    public AudioSource audioSource;
    public AudioClip buttonClip;

    private void Update() {
        
        DropOnPickUp();
        UpdateText();

        if (!activeParticleSystem) return;

        if (item) {
            if (!beaker) beaker = item.GetComponent<Beaker>();
            if (!complete) {
                int direction = 1;
                if (temperature == Ingredient.Temperature.Cold)
                    direction = -1;
                beaker.temperature += direction * Time.deltaTime;

                bool tempComplete;

                if (temperature == Ingredient.Temperature.Cold)
                    tempComplete = beaker.temperature < -tempLimit;
                else
                    tempComplete = beaker.temperature > tempLimit;

                if (!tempComplete)
                {
                    beaker.Complete(Ingredient.Temperature.None);
                }

                        
                if (tempComplete) {
                    //beaker.ingredient.temperature = temperature;
                    beaker.Complete(temperature);
                    complete = true;
                }
                else
                {
                    beaker.ingredient.temperature = Ingredient.Temperature.None;
                }
                if (!activeParticleSystem.isPlaying)
                activeParticleSystem.Play();
            }
            else
            {
                activeParticleSystem.Stop();
            }
        } else {
            beaker = null;
            complete = false;
            activeParticleSystem.Stop();
        }
    }

    public void UpdateText() {
        setTempText.text = "";
        if (temperature == Ingredient.Temperature.Cold)
            setTempText.text = "-";
        setTempText.text += "100°";

        float setTempLimit = tempLimit;
        if (temperature == Ingredient.Temperature.Cold)
            setTempLimit = tempLimit;

        if (beaker) {
            tempText.text = Mathf.Round((beaker.temperature / setTempLimit) * 100).ToString();
        } else
            tempText.text = "0";
        tempText.text += "°";
    }

    public void SetTemperature(int temperatureType) {
        temperature = (Ingredient.Temperature)temperatureType;
        if (activeParticleSystem) activeParticleSystem.Stop();
        activeParticleSystem = particleSystems[temperatureType-1];
        complete = false;
    }

    public void SetButton(GameObject button) {
        if (activeButton) {
            Renderer preActiveRenderer = activeButton.GetComponent<Renderer>();
            preActiveRenderer.material.SetColor("_EmissionColor", Color.black);
        }
        activeButton = button;
        Renderer activeRenderer = activeButton.GetComponent<Renderer>();
        activeRenderer.material.SetColor("_EmissionColor", activeRenderer.material.color);
        Manager.PlayAudio(audioSource, buttonClip);
    }
}
