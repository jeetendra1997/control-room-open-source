﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger
{
    bool _state;
    public bool state { get { bool value = _state; _state = false; return value; } set { _state = value; } }


    public void Set(bool state)
    {
        this.state = state;
    }

    public static implicit operator bool(Trigger trigger)
    {
        return trigger.state;
    }
}