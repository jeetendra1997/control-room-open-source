﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource))]
public class AudioPlayer : MonoBehaviour {

    public AudioSource audioSource;
    public bool destroyAfter;
    public bool playOnTrigger;
    float destroyTime;
    public Vector2 pitchRange = Vector2.one;

    private void Start()
    {
        audioSource.Stop();        
        audioSource.outputAudioMixerGroup = GlobalManager.GameDef.GetMixer();
    }

    public void Play(AudioClip audioClip, Vector2 pitchRange, float spatialBlend, float maxDistance)
    {        
        gameObject.SetActive(true);
        audioSource.Stop();
        audioSource.clip = audioClip;
        audioSource.maxDistance = maxDistance;
        if (pitchRange != Vector2.zero)
            this.pitchRange = pitchRange;
        if (spatialBlend != 0)
            audioSource.spatialBlend = spatialBlend;
        audioSource.Play();        
        if (audioSource.clip)
        destroyTime = Time.timeSinceLevelLoad + audioSource.clip.length;        
    }

    void Update () {
        if (destroyAfter)
            if (Time.timeSinceLevelLoad > destroyTime && !audioSource.isPlaying)
                Reset();
                //Destroy(gameObject);
            //if (!audioSource.isPlaying) Destroy(gameObject);
	}

    void Reset()
    {
        gameObject.SetActive(false);
        audioSource.volume = 1;
        audioSource.spatialBlend = 1;
        audioSource.pitch = 1;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Floor"))
        {
            audioSource.pitch = Random.Range(pitchRange.x, pitchRange.y);
            audioSource.Play();
        }
    }
}
