﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Difficulty
{
    Easy, Medium, Hard, Insane
}
public enum Level
{
    Sanction, Concentrate, KillRoom, Tutorial, Factor, Crest,
}

[System.Serializable]
public class SoldierInfo
{
    public enum SoldierClass
    {
        Assault, Engineer
    }

    public List<SoldierClass> soldierClasses = new List<SoldierClass>();
}

[System.Serializable]
public class LevelInfo {

    public SoldierInfo[] soldierInfos = new SoldierInfo[4];
    public int scene;
    public int difficulty;
    public Level level;
    public Sprite preview;

    public static int[] multiplayerMaps = new int[1]{4 };

    public static Level GetMultiplayerMap(int value)
    {
        return (Level)multiplayerMaps[value];
    }
}
