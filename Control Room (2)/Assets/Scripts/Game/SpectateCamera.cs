﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpectateCamera : Singleton<SpectateCamera> {

    public bool active;
    new Camera camera;
    public Vector3 cameraEuler;
    public Light[] lights;
    bool freeze;

    int multiplier = 1;
    float speed;


    private void Start()
    {
        if (Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(this);
        camera = GetComponent<Camera>();
        camera.enabled = false;     
    }

    // Update is called once per frame
    void Update () {

        if (PauseMenu.paused) return;
        if (Input.GetKeyDown(KeyCode.F1) && Application.isEditor)
        {
            active = !active;
            camera.enabled = active;
        }

        if (!active) return;

        if (Input.GetKeyDown(KeyCode.F2))
            freeze = !freeze;

        if (!freeze)
            Time.timeScale = 1;
        else Time.timeScale = 0;

        Speed();
        Lights();
        CameraRotation();
        Movement();
	}

    void Speed()
    {
        if (Input.GetKeyDown(KeyCode.V))
            multiplier = 1;
        if (Input.GetKeyDown(KeyCode.B))
            multiplier = 3;
        if (Input.GetKeyDown(KeyCode.N))
            multiplier = 5;
        if (Input.GetKeyDown(KeyCode.M))
            multiplier = 7;
        speed = 0.05f * multiplier;
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            multiplier += 2;
            if (multiplier > 4 * 2)
                multiplier = 1;
        }
    }

    void Lights()
    {
        if (Input.GetKeyDown(KeyCode.F3))
            SetActiveLight(0);
        if (Input.GetKeyDown(KeyCode.F4))
            SetActiveLight(1);

        for (int i = 0; i < lights.Length; i++)
        {
            if (Input.GetKey(KeyCode.KeypadPlus))
                lights[i].range += speed;
            if (Input.GetKey(KeyCode.KeypadMinus))
                lights[i].range -= speed;
            if (Input.GetKey(KeyCode.KeypadMultiply))
                lights[i].intensity += speed;
            if (Input.GetKey(KeyCode.KeypadDivide))
                lights[i].intensity -= speed;
            if (Input.GetKey(KeyCode.PageUp))
                lights[i].spotAngle += speed;
            if (Input.GetKey(KeyCode.PageDown))
                lights[i].spotAngle -= speed;
        }
    }

    void SetActiveLight(int index)
    {
        for(int i = 0; i < lights.Length; i++)
        {
            if (index == i)
                lights[i].enabled = true;
            else
                lights[i].enabled = false;
        }
    }

    void Movement()
    {
        Debug.Log(Input.GetAxis("Horizontal"));
        Vector3 movement = Vector3.ClampMagnitude(new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")), 1);
        transform.position += transform.rotation * movement * speed;

        if (Input.GetKey(KeyCode.Space))
            transform.position += Vector3.up * speed;
        if (Input.GetKey(KeyCode.LeftControl))
            transform.position += Vector3.down * speed;
        
    }

    void CameraRotation()
    {        
        float mouseSensitivity = GlobalSettings.mouseSensitivity;        
        
        cameraEuler.y += Input.GetAxisRaw("Mouse X") * mouseSensitivity;
        cameraEuler.x -= Input.GetAxisRaw("Mouse Y") * mouseSensitivity;
        if (cameraEuler.x > 70) cameraEuler.x = 70;
        if (cameraEuler.x < -70) cameraEuler.x = -70;

        //transform.rotation = Quaternion.Euler(new Vector3(0, cameraEuler.y, 0));
        //Vector3 cameraShake = new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), Random.Range(-1, 1)) * (audioSource.volume * 0.1f);
        transform.rotation = Quaternion.Euler(cameraEuler);

        camera.fieldOfView += Input.mouseScrollDelta.y * speed;
    }
}
