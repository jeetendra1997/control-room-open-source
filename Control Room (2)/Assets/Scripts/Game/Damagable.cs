﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DamageMask
{ 
    Infected, Soldier, Environment
}

[System.Serializable]
public class DamageInfo
{                
    public List<DamageMask> damageMasks;
    public float damage;
    public Vector3 direction;
    public Character attacker;
    public Limb limb;

    public static DamageInfo Copy(DamageInfo copy)
    {
        DamageInfo newInfo = new DamageInfo();
        newInfo.damageMasks = copy.damageMasks;
        newInfo.damage = copy.damage;
        newInfo.direction = copy.direction;
        newInfo.attacker = copy.attacker;
        newInfo.limb = copy.limb;
        return newInfo;
    }

    public static DamageInfo GetInfo(Character character, int attackerId, float damage, Vector3 direction, int limbId)
    {
        Character attacker = Manager.Instance.GetBehaviour<Character>(attackerId);
        Limb limb = character.GetLimb(limbId);
        return new DamageInfo(damage, direction, attacker, limb);
    }
    
    public DamageInfo()
    {

    }

    public void Assign(float damage, Vector3 direction, Character attacker, Limb limb)
    {
        this.damage = damage;
        this.direction = direction;
        this.attacker = attacker;
        this.limb = limb;
    }

    public DamageInfo(float damage, Vector3 direction, Character attacker, Limb limb)
    {
        Assign(damage, direction, attacker, limb);
    }
}

public class Damagable : MonoBehaviour {

    public List<DamageMask> damageMasks;
	public virtual void Damage(DamageInfo damage)
    {

    }
}
