﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatBox : MultiplayerBehaviour
{
    public Text chatText;
    public InputField chatInput;

    public bool stoppedFocus;

    private void OnEnable()
    {
        chatText.text = "";
    }

    private void Update()
    {
        ChatInput();
    }

    public void ChatInput()
    {
        if (chatInput.isFocused)
        {
            stoppedFocus = true;            
        }
        else if (stoppedFocus)
        {
            if ((Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter)) && chatInput.text.Length > 0)
            {                
                AddMessage(PhotonNetwork.player.NickName, chatInput.text);
                chatInput.text = "";
                chatInput.ActivateInputField();
            }
            stoppedFocus = false;
        }
    }

    public void AddMessage(string username, string text)
    {
        text = GlobalManager.FilterText(text);
        if (PhotonNetwork.inRoom)
            photonView.RPC("RPCAddMessage", PhotonTargets.AllViaServer, username, text);
        else
            RPCAddMessage(username, text);
    }

    [PunRPC]
    public virtual void RPCAddMessage(string username, string text)
    {
        chatText.text += "\n";
        chatText.text += "<color=white>" + username + ": </color>";
        chatText.text += text;
    }
}