﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSource : Power {

    public bool emergencyBackUp;

    public Light light;
    public Color defaultColor;

    private void Update()
    {
        BackUp();
    }

    void BackUp()
    {

    }

    public override void Enable(bool state)
    {
        if (!state)
            StartCoroutine(StartBackUp());
    }

    public IEnumerator StartBackUp()
    {
        yield return null;
    }
}
