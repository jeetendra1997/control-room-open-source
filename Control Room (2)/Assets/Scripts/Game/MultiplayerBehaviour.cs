﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PhotonView))]
public class MultiplayerBehaviour : MonoBehaviour {

    public PhotonView _photonView;
    //public PhotonView photonView { get {  if (!_photonView) _photonView = GetComponent<PhotonView>(); if (!_photonView && PhotonNetwork.inRoom) Debug.Log(gameObject.name + " is missing PhotonView"); return _photonView; } }
    public PhotonView photonView { get { if (!_photonView) _photonView = GetComponent<PhotonView>(); return _photonView; } }

    public int id;

    private void Awake()
    {
        RegisterID();
    }

    public void RegisterID()
    {
        //Manager.Instance.multiplayerBehaviours.Add(this);
        Manager.Instance.RegisterID(this);
    }
}
