﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleEmitter : MonoBehaviour {

    public DestroyDelay[] particles;
    public bool playing;

    private void Start()
    {
        foreach(DestroyDelay particle in particles)
        {
            particle.emitter = this;
            particle.gameObject.SetActive(false);
        }
    }

    public bool Play(ParticleType type)
    {
        if (playing) return false;
        foreach(DestroyDelay particle in particles)
        {
            if (particle.type == type)
            {
                if (particle.gameObject.activeSelf)
                {
                    return false;
                }
                else
                {
                    particle.Play();
                    return true;
                }
            }
        }
        return false;
    }
}
