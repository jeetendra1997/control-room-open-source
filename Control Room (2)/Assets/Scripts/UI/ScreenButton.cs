﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenButton : WorldButton {

    public Color onColor;
    public Color offColor;

    public void SetColor(Color color)
    {
        renderer.material.color = color;
        renderer.material.SetColor("_EmissionColor", color);
    }
}
