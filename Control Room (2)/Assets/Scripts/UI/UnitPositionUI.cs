﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitPositionUI : MonoBehaviour {

    bool unitSet;    
    public MilitaryUnit militaryUnit;    
	
	// Update is called once per frame
	void Update () {
        if (!unitSet)
        {
            militaryUnit = MilitaryManager.GetUnit(militaryUnit.unitCode);
            GetComponent<Renderer>().material.color = militaryUnit.color;
            unitSet = true;
        }
        
        Vector3 position = militaryUnit.GetCurrentPosition();
        if (militaryUnit.soldiers.Count == 0)
        {
            Destroy(gameObject);
            return;
        }
        position.y = 9;
        transform.position = position;        
    }
}
