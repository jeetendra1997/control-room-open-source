﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryHUD : Singleton<InventoryHUD> {
    
    public Text ammoText;
    public InventoryHUDSlot activeSlot;
    public InventoryHUDSlot[] slots;

    public Character character;

    private void Update()
    {
        if (activeSlot == null || !activeSlot.item)
            ammoText.gameObject.SetActive(false);
        else
        {
            Gun gun = activeSlot.item.GetComponent<Gun>();
            if (gun)
            {
                ammoText.gameObject.SetActive(true);
                //ammoText.text = gun.ammo.ToString() + "/" + gun.clipSize.ToString();
                ammoText.text = gun.ammo.ToString() + "/" + character.GetAmmo(gun.def.type).amount.ToString();
            }
        }
    }

    public void UpdateHUD(Player player)
    {
        SetActiveSlot(-1);
        for (int i = 0; i < slots.Length; i++)
        {
            Item item = player.inventory.slots[i].item;
            SetSlot(i, item);
            if (item && item.gameObject.activeSelf)
                SetActiveSlot(i);
            
        }
    }

    public void UpdateImage(Item item, Sprite image)
    {
        foreach(InventoryHUDSlot slot in slots)
        {
            if (slot.item == item)
            {
                slot.itemImage.sprite = image;
            }
        }
    }

    public void SetSlot(int index, Item item)
    {
        if (item)
        {
            slots[index].item = item;
            slots[index].itemImage.sprite = item.image;
            slots[index].itemImage.gameObject.SetActive(true);
        }
        else
        {
            slots[index].item = null;
            slots[index].itemImage.sprite = null;
            slots[index].itemImage.gameObject.SetActive(false);
            slots[index].Unselect();
        }
    }

    public void SetActiveSlot(int index)
    {     
        if (activeSlot)
            activeSlot.Unselect();
        if (index < 0) return;
        activeSlot = slots[index];
        activeSlot.Select();
    }

    public void UnsetActiveSlot()
    {
        if (activeSlot)
            activeSlot.Unselect();
        activeSlot = null;
    }
}
