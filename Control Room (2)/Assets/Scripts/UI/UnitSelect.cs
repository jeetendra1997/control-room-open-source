﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitSelect : MonoBehaviour {

    public GameObject[] units;
    int unitCount = 4;

    public void AssignUnitSize()
    {
        foreach(GameObject unit in units)
        {
            for (int i = 0; i < unit.transform.childCount; i++)
            {
                if (i < 4 - GlobalManager.levelInfo.difficulty)
                //if (i < unitCount)
                    unit.transform.GetChild(i).gameObject.SetActive(true);
                else
                    unit.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
        UpdateUnits();
    }

    public void UpdateUnits()
    {
        for (int i = 0; i < units.Length; i++)
        {
            GlobalManager.levelInfo.soldierInfos[i] = new SoldierInfo();
            for (int y = 0; y < 4 - GlobalManager.levelInfo.difficulty; y++)
            //for (int y = 0; y < unitCount; y++)
            {
                int value = units[i].transform.GetChild(y).GetComponent<Dropdown>().value;
                GlobalManager.levelInfo.soldierInfos[i].soldierClasses.Add((SoldierInfo.SoldierClass)value);
            }
        }
    }
}
