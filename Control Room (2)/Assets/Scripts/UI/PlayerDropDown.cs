﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerDropDown : MonoBehaviour
{
    public Dropdown dropDown;
    public Text nameText;
    public PhotonPlayer player;
    public bool admin;
    int childCount;
    bool open;    

    private void Update()
    {
        if (dropDown.transform.childCount > childCount)
            open = true;
        else
        {
            if (open)
            {
                InputValue(dropDown.value);
            }
            open = false;
        }
    }

    public void Assign(PhotonPlayer player)
    {
        this.player = player;
        nameText.text = player.NickName;

        admin = PhotonNetwork.isMasterClient;
        childCount = dropDown.transform.childCount;
        dropDown.ClearOptions();
        List<string> options = new List<string>();
        options.Add(player.NickName);
        if (admin && player != PhotonNetwork.player)
            options.Add("Kick");
        dropDown.AddOptions(options);
    }    

    public void InputValue(int value)
    {
        Debug.Log(value);
        if (admin)
        {
            if (value == 1)
                Kick();
        }        
    }

    public void Kick()
    {
        //Debug.Log(PhotonNetwork.CloseConnection(player));
        RoomMenu.Kick(player);
    }
}
