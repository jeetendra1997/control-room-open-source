﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPositionUI : Interactable {

    public Player player;
    public Color color;

    private void Start() {
        GetComponent<WorldButton>().Event.AddListener(delegate { CRInterface.Instance.ViewPlayerInfo(player); });
    }

    public void AssignPlayer(Player player, Color color) {
        Renderer rend = GetComponent<Renderer>();        
        Vector3 colorVector = new Vector3(color.r, color.g, color.b);
        if (PhotonNetwork.inRoom) {
            photonView.RPC("RPCSetColor", PhotonTargets.AllBuffered, player.id, colorVector);
        } else {
            RPCSetColor(player.id, colorVector);
        }
    }

    [PunRPC]
    public void RPCSetColor(int playerId, Vector3 colorVector) {
        Renderer rend = GetComponent<Renderer>();
        Color newColor = new Color(colorVector.x, colorVector.y, colorVector.z, 1);
        color = newColor;
        rend.material.color = newColor;
        player = Manager.Instance.GetBehaviour<Player>(playerId);
        player.positionUI = this;
    }

    void Update () {
        if (player == null) {
            Destroy(gameObject);
            return;
        }
        Position();
	}

    void Position() {
        Vector3 position = player.transform.position;
        position.y = 9;
        transform.position = position;


    }
}
