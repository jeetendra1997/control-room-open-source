﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameChatBox : ChatBox {

    public RectTransform chatBacking;
    public Image chatInputBacking;
    public Color startColor;
    bool active;

    float timeDelay;

    private void OnEnable() {
        startColor = chatInputBacking.color;
        SetChatActive(false);
    }

    private void Update() {
        ChatInput();

        if (Input.GetKeyDown(KeyCode.Return)) {
            SetChatActive(!active);
        }

        if (timeDelay > 0)        
            timeDelay -= Time.deltaTime;
        else if (!active)
        {
            chatText.enabled = false;
        }
    }

    public void SetChatActive(bool state) {
        chatText.enabled = true;
        active = state;
        GlobalManager.chatting = state;

        chatInput.gameObject.SetActive(state);
        float offset = 100;        

        if (!state) {
            Color newColor = startColor;
            newColor.a = 0.01f;
            chatBacking.position += Vector3.down * offset / 2;
            foreach (RectTransform trans in chatBacking.GetComponentsInChildren<RectTransform>()) {
                trans.sizeDelta += Vector2.down * offset;                
            }
            chatBacking.GetComponent<Image>().color = newColor;
            chatInputBacking.color = newColor;            
        } 
        else {
            chatBacking.position -= Vector3.down * offset / 2;
            foreach (RectTransform trans in chatBacking.GetComponentsInChildren<RectTransform>()) {
                trans.sizeDelta -= Vector2.down * offset;                
            }
            chatBacking.GetComponent<Image>().color = startColor;
            chatInputBacking.color = startColor;
            chatInput.ActivateInputField();            
        }
    }

    [PunRPC]
    public override void RPCAddMessage(string username, string text)
    {
        base.RPCAddMessage(username, text);
        timeDelay = 5;
        chatText.enabled = true;
    }
}
