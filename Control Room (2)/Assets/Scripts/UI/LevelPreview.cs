﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelPreview : MonoBehaviour {

    public int scene;
    public Image previewImage;
    public LevelSelect levelSelect;
    public Level level;
    public Text nameText;

    public void SelectLevel()
    {
        levelSelect.SelectLevel(this);
    }

    public void Hover()
    {
        MenuManager.Instance.TriggerHover();
    }

    public void Click()
    {
        MenuManager.Instance.TriggerClick();
    }
}
