﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Analytics;

public class ConnectionWindow : MonoBehaviour
{
    public UnityEvent onConnect;
    public Text connectingText;
    public GameObject backButton;

    public void JoinLobby()
    {        
        Debug.Log("joinLobby");
        StartCoroutine("Join");        
    }

    public void Disconnect()
    {
        backButton.SetActive(false);
        StartCoroutine(DisconnectFromRoom());
    }

    IEnumerator DisconnectFromRoom()
    {
        while (PhotonNetwork.inRoom)
            yield return null;
        backButton.SetActive(true);
    }

    IEnumerator Join()
    {
        if (PhotonNetwork.connectionState == ConnectionState.Disconnected)       
            PhotonNetwork.ConnectUsingSettings(GlobalManager.Instance.gameVersion.ToString());
        if (PhotonNetwork.connectionState != ConnectionState.Connected)
        {
            Analytics.CustomEvent("ConnectOnline", new Dictionary<string, object>
            {
                { "playerID", GlobalManager.playerID },
                { "gameVersion", GlobalManager.Instance.gameVersion }
            });

            while (!PhotonNetwork.connectedAndReady)
            {
                yield return null;
            }
            while (!PhotonNetwork.JoinLobby())
            {
                yield return new WaitForSeconds(0.5f);
                Debug.Log(PhotonNetwork.connectionState);
                yield return null;
            }
        }        
        onConnect.Invoke();
        while (!PhotonNetwork.insideLobby && !PhotonNetwork.connected)
        {
            yield return null;
        }
        yield return null;
        GlobalManager.connected = true;
    }

    void OnConnectedToMaster()
    {
        Debug.Log("connected");
        onConnect.Invoke();        
    }

}