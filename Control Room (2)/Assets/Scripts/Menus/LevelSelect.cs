﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class LevelSelect : MonoBehaviour {

    public LevelPreview previewPrefab;
    public Transform previewPosition;
    public UnityEvent onLevelSelect;
    public List<LevelPreview> previews = new List<LevelPreview>();

	public void LoadLevels(MapSelect mapSelect)
    {
        foreach(LevelPreview preview in previews)
        {
            Destroy(preview.gameObject);
        }
        previews = new List<LevelPreview>();

        for (int i = 0; i < mapSelect.mapSelectLevels.Length; i++)
        {            
            LevelPreview preview = Instantiate(previewPrefab.gameObject, transform.position, Quaternion.identity, transform).GetComponent<LevelPreview>();
            Vector3 offset = Vector3.right * (i * 180);
            RectTransform rectTransform = preview.GetComponent<RectTransform>();
            rectTransform.position = previewPosition.position;
            rectTransform.anchoredPosition += (Vector2)offset;
            preview.previewImage.sprite = mapSelect.mapSelectLevels[i].previewImage;
            preview.level = mapSelect.mapSelectLevels[i].level;
            preview.nameText.text = System.Enum.GetName(typeof(Level), mapSelect.mapSelectLevels[i].level); 
            preview.scene = mapSelect.mapSelectLevels[i].scene;
            preview.levelSelect = this;
            previews.Add(preview);
            preview.GetComponent<Button>().onClick.AddListener(MenuManager.Instance.TriggerClick);
        }
    }

    public void SelectLevel(LevelPreview levelPreview)
    {
        GlobalManager.levelInfo.scene = levelPreview.scene;
        GlobalManager.levelInfo.level = levelPreview.level;
        onLevelSelect.Invoke();
    }
}
