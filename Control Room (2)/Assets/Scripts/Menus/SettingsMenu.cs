﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour {

    public Text mouseSensitivityText;
    public Text ragdollLimitText;
    public Text masterVolumeText;

    public Slider mouseSensitivity;
    public Slider ragdollLimit;
    public Slider masterVolume;
    public Dropdown characterQuality;
    public Dropdown lightQuality;
    public Dropdown postProcessingQuality;
    public Dropdown motionBlur;

    void Start () {        
        mouseSensitivity.value = GlobalSettings.mouseSensitivity;
        ragdollLimit.value = GlobalSettings.ragdollLimit;
        masterVolume.value = GlobalSettings.masterVolume;
        characterQuality.value = (int)GlobalSettings.characterQuality;
        lightQuality.value = (int)GlobalSettings.lightQuality;
        postProcessingQuality.value = (int)GlobalSettings.postProcessingQuality;

        motionBlur.value = System.Convert.ToInt32(GlobalSettings.motionBlur) == 0 ? 1 : 0;
    }
	
	// Update is called once per frame
	void Update () {
        mouseSensitivityText.text = (Mathf.Round(mouseSensitivity.value * 10)/10).ToString();
        ragdollLimitText.text = ((int)ragdollLimit.value).ToString();
        masterVolumeText.text = ((int)masterVolume.value).ToString();
	}

    public void ApplyMouseSensitivity(float amount)
    {
        GlobalSettings.SetKey("mouseSensitivity", amount);
        GlobalSettings.mouseSensitivity = amount;
    }

    public void ApplyRagdollLimit(float amount)
    {
        GlobalSettings.SetKey("ragdollLimit", amount);
        GlobalSettings.ragdollLimit = (int)amount;
    }

    public void ApplyCharacterQuality(int value)
    {
        GlobalSettings.SetKey("characterQuality", value);
        GlobalSettings.characterQuality = (GlobalSettings.QualityLevel)value;
    }

    public void ApplyLightQuality(int value)
    {
        GlobalSettings.SetKey("lightQuality", value);
        GlobalSettings.lightQuality = (GlobalSettings.QualityLevel)value;
    }

    public void ApplyMasterVolume(float value)
    {
        GlobalSettings.SetKey("masterVolume", value);
        GlobalSettings.masterVolume = value;
    }

    public void ApplyMotionBlur(int value)
    {
        value = value == 0 ? 1 : 0;
        GlobalSettings.SetKey("motionBlur", value);
        GlobalSettings.motionBlur = System.Convert.ToBoolean(value);
        GlobalSettings.ApplySettings();
    }

    public void ApplyPostProcessingQuality(int value)
    {
        GlobalSettings.SetKey("postProcessingQuality", value);
        GlobalSettings.postProcessingQuality = (GlobalSettings.QualityLevel)value;
        GlobalSettings.ApplySettings();
    }
}
