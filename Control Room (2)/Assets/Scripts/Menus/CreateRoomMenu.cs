﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateRoomMenu : MonoBehaviour {

    public InputField roomNameTextInput;
    public InputField passwordTextInput;
    public Dropdown mapDropDown;
    public Dropdown difficultyDropDown;
    public Slider maxPlayersSlider;    
    public Image passwordProtectedImage;
    public bool passwordProtected;
    public GameObject roomMenu;
    public Text errorText;
    bool creatingRoom;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
            CreateRoom();
    }

    public void CreateRoom()
    {
        if (creatingRoom)
        {
            errorText.text = "Creating room. Please wait.";
            return;
        }
        if (roomNameTextInput.text.Length < 1)
        {
            errorText.text = "Name is too short.";
            return;
        }
        if (GlobalManager.ContainsFilteredWord(roomNameTextInput.text))
        {
            errorText.text = "Contains banned word.";
            return;
        }
        if (MultiplayerManager.RoomExists(roomNameTextInput.text))
        {
            errorText.text = "Room name already taken.";
            return;
        }

        StartCoroutine(CreateRoomCheck());
    }
    
    IEnumerator CreateRoomCheck()
    {
        creatingRoom = true;
        if (PhotonNetwork.inRoom)
        {
            Debug.Log(PhotonNetwork.inRoom);
            PhotonNetwork.LeaveRoom();
            while (PhotonNetwork.inRoom)
            {
                Debug.Log(PhotonNetwork.inRoom);
                yield return null;
            }
        }
        if (!PhotonNetwork.insideLobby)
        {
            PhotonNetwork.JoinLobby();
            while (!PhotonNetwork.insideLobby)
            {
                Debug.Log(PhotonNetwork.insideLobby);
                yield return null;
            }
        }

        //bool roomCreated = MultiplayerManager.CreateRoom(roomNameTextInput.text, LevelInfo.GetMultiplayerMap(mapDropDown.value), passwordProtected, passwordTextInput.text, System.Convert.ToByte((int)maxPlayersSlider.value));
        bool roomCreated = MultiplayerManager.CreateRoom(roomNameTextInput.text, passwordProtected, passwordTextInput.text, System.Convert.ToByte((int)maxPlayersSlider.value));
        Debug.Log(roomCreated);
        if (roomCreated)
            MenuManager.Instance.OpenMenu(roomMenu);
        else
            errorText.text = "Failed to create room. Please try again.";
        creatingRoom = false;        
    }

    public void SetMap(int dropDownIndex)
    {
        int i = (int)Level.Factor + dropDownIndex;
        LevelInfo info = GlobalManager.GameDef.GetLevel((Level)i);
        GlobalManager.levelInfo.level = info.level;
        GlobalManager.levelInfo.scene = info.scene;
        if (mapDropDown.value != dropDownIndex)
            mapDropDown.value = dropDownIndex;
        Debug.Log(GlobalManager.levelInfo.level);
    }

    public void SetDifficulty(int index)
    {
        GlobalManager.levelInfo.difficulty = index;
        difficultyDropDown.value = index;
    }

    private void OnEnable()
    {
        errorText.text = "";
        //roomNameTextInput.ActivateInputField();
        mapDropDown.ClearOptions();        
        List<string> maps = new List<string>();        
        for (int i = (int)Level.Factor; i < (int)Level.Crest + 1; i++) {
            maps.Add((System.Enum.GetName(typeof(Level), i)));            
                }
        mapDropDown.AddOptions(maps);
        SetMap(0);
        mapDropDown.onValueChanged.AddListener(SetMap);        

        difficultyDropDown.ClearOptions();
        List<string> difficulties = new List<string>();
        for (int i = (int)0; i < (int)Difficulty.Insane + 1; i++)
        {
            difficulties.Add((System.Enum.GetName(typeof(Difficulty), i)));

        }
        difficultyDropDown.AddOptions(difficulties);
        difficultyDropDown.onValueChanged.AddListener(SetDifficulty);
        SetDifficulty((int)Difficulty.Medium);

        StartCoroutine("SetTextActive");
    }

    IEnumerator SetTextActive()
    {
        yield return null;
        roomNameTextInput.ActivateInputField();
    }

    public void SetPasswordProtect()
    {
        passwordProtected = !passwordProtected;
        passwordProtectedImage.gameObject.SetActive(passwordProtected);
        passwordTextInput.gameObject.SetActive(passwordProtected);
        if (passwordProtected)
            passwordTextInput.ActivateInputField();        
    }
}
