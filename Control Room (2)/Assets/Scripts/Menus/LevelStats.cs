﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;

public class LevelStats : MonoBehaviour {

    public Text survivorText;
    public Text statText;

    public void LoadStats()
    {
        if (PhotonNetwork.inRoom)
        {            
            if (PhotonNetwork.isMasterClient)
            {
                StartCoroutine(LeaveRoom());
                //MultiplayerManager.UpdateCustomProperty(PhotonNetwork.room, CustomPropertyType.InGame, false);
                //MultiplayerManager.UpdateCustomProperty(PhotonNetwork.room, CustomPropertyType.Starting, false);                
            }
            else
            {
                PhotonNetwork.LeaveRoom();
            }
        }

        if (GlobalManager.survived)
            survivorText.text = "Survivor";
        else
            survivorText.text = "Dead";

        AddAnalytics(GlobalManager.survived);

        statText.text = "";        

        float time = GlobalManager.totalTime;
        Vector3 timeVector = new Vector3(Mathf.Floor(time / 60), Mathf.RoundToInt(time % 60), Mathf.RoundToInt((time * 1000) % 1000));

        AddLine(System.Enum.GetName(typeof(Level), GlobalManager.levelInfo.level).ToUpper());
        AddLine("");
        AddLine(System.Enum.GetName(typeof(Difficulty), (Difficulty)GlobalManager.levelInfo.difficulty).ToUpper());
        AddLine("");

        if (!PhotonNetwork.inRoom) {                        
            AddLine("Total time: " + System.String.Format("{0:00}:{1:00}", timeVector.x, timeVector.y));
            AddLine("Soldiers alive: " + (GlobalManager.totalSoldiers - GlobalManager.deadSoldiers) + "/" + GlobalManager.totalSoldiers);
            AddLine("Infected killed: " + (GlobalManager.deadInfected));
            AddLine("");
        }

        else {
            //StartCoroutine(LeaveRoom());
            if (GlobalManager.hasCure)
                AddLine("Escaped with cure.");
            else if (GlobalManager.survived)
                AddLine("Escaped without cure.");
            AddLine("");

            AddLine("Total time: " + System.String.Format("{0:00}:{1:00}", timeVector.x, timeVector.y));
            AddLine("Players alive: " + (GlobalManager.playersSurvived));
            AddLine("Infected killed: " + (GlobalManager.deadInfected));
            AddLine("");
        }

        LevelCompletion level = new LevelCompletion(GlobalManager.levelInfo.level, (Difficulty)GlobalManager.levelInfo.difficulty, GlobalManager.hasCure);

        if (!GlobalManager.survived) return;

        MusicManager musicManager = MusicManager.Instance;
        if (musicManager)
        {
            foreach (UnlockableTrack track in musicManager.tracks)
            {                
                if (!track.CheckUnlocked())
                {                    
                    if (track.Unlock(level))
                    {
                        AddLine("Track unlocked: " + track.name);
                        musicManager.PlayTrack(track);
                    }
                }
            }
        }
    }

    public IEnumerator LeaveRoom() {
        string name = PhotonNetwork.room.Name;
        int maxPlayers = PhotonNetwork.room.MaxPlayers;
        PhotonNetwork.LeaveRoom();        
        
        float delay = 5;
        while (PhotonNetwork.inRoom && delay > 0) {
            //Debug.Log(PhotonNetwork.inRoom);
            delay -= Time.deltaTime;
            yield return null;
        }
        if (!PhotonNetwork.insideLobby)
            PhotonNetwork.JoinLobby();
        
        while (!PhotonNetwork.insideLobby && delay > 0) {
            delay -= Time.deltaTime;
            yield return null;
        }
        
        Debug.Log(PhotonNetwork.insideLobby);

        Debug.Log(MultiplayerManager.CreateRoom(name, false, "", System.Convert.ToByte(maxPlayers)));

    }

    public void AddLine(string text)
    {
        statText.text += text + "\n";

    }

    public void AddAnalytics(bool survived)
    {        
        if (survived)
            Analytics.CustomEvent("Survived", new Dictionary<string, object>
            {
                { "levelName", System.Enum.GetName(typeof(Level), GlobalManager.levelInfo.level) + " " + GlobalManager.playerID },
                { "difficulty", System.Enum.GetName(typeof(Difficulty), GlobalManager.levelInfo.difficulty) + " " + GlobalManager.playerID},
                { "totalTime", GlobalManager.totalTime.ToString() + " " + GlobalManager.playerID },
                { "soldiers alive", GlobalManager.deadSoldiers.ToString() + " / " + GlobalManager.totalSoldiers.ToString() + " " + GlobalManager.playerID},
                { "infected killed", GlobalManager.deadInfected.ToString() + " / " + GlobalManager.totalInfected.ToString() + " " + GlobalManager.playerID},
                { "transmissions repaired", GlobalManager.totalTransmissionRepairs.ToString() + " " +  GlobalManager.playerID }
            });
        else
            Analytics.CustomEvent("Killed", new Dictionary<string, object>
            {
                { "levelName", System.Enum.GetName(typeof(Level), GlobalManager.levelInfo.level) + " " + GlobalManager.playerID },
                { "difficulty", System.Enum.GetName(typeof(Difficulty), GlobalManager.levelInfo.difficulty) + " " + GlobalManager.playerID},
                { "totalTime", GlobalManager.totalTime.ToString() + " " + GlobalManager.playerID },
                { "soldiers alive", GlobalManager.deadSoldiers.ToString() + " / " + GlobalManager.totalSoldiers.ToString() + " " + GlobalManager.playerID},
                { "infected killed", GlobalManager.deadInfected.ToString() + " / " + GlobalManager.totalInfected.ToString() + " " + GlobalManager.playerID},
                { "transmissions repaired", GlobalManager.totalTransmissionRepairs.ToString() + " " +  GlobalManager.playerID }
            });
    }
}
