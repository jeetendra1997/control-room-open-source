﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class NicknameMenu : MonoBehaviour {

    public InputField nameInputField;
    public Text errorText;
    public UnityEvent continueEvent;

    private void OnEnable()
    {
        errorText.text = "";
        string name = PhotonNetwork.player.NickName;
        if (name == null || name == "")
        {
            nameInputField.ActivateInputField();
            return;
        }
        continueEvent.Invoke();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return))
            Continue();
    }

    public void Continue()
    {
        string name = nameInputField.text;
        if (name.Length < 1)
            errorText.text = "Name too short.";
        else if (GlobalManager.ContainsFilteredWord(name))
            errorText.text = "Contains banned word.";
        else
        {
            PhotonNetwork.player.NickName = name;
            continueEvent.Invoke();
        }

    }    
}
