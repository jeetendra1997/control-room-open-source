﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitStatBarMenu : MultiplayerBehaviour {

    public MilitaryUnit unit;
    public GameObject unitBarPrefab;
    public List<UnitStatBarUI> unitBars;    

    private void Update()
    {
        if (unit == null) return;
        UnitBars();
    }

    public void AssignPlayer(Player player) {
        for (int i = 0; i < base.transform.childCount; i++) {
            Destroy(base.transform.GetChild(i).gameObject);
        }
        unitBars = new List<UnitStatBarUI>();

        if (!PhotonNetwork.inRoom) {
            UnitStatBarUI statBar = Instantiate(unitBarPrefab, transform.position, transform.rotation).GetComponent<UnitStatBarUI>();
            AssignPlayerStatBar(statBar, player);
        }
        else if (PhotonNetwork.isMasterClient) {
            UnitStatBarUI statBar = PhotonNetwork.Instantiate(unitBarPrefab.gameObject.name, transform.position, transform.rotation,0).GetComponent<UnitStatBarUI>();
            AssignPlayerStatBar(statBar, player);
        }


        UpdateBars();
    }

    public void AssignPlayerStatBar(UnitStatBarUI statBar, Player player) {
     if (PhotonNetwork.inRoom) {
            photonView.RPC("RPCAssignPlayerStatBar", PhotonTargets.AllBuffered, statBar.id, player.id);
        } else {
            RPCAssignPlayerStatBar(statBar.id, player.id);
        }
    }

    [PunRPC]
    public void RPCAssignPlayerStatBar(int statBarId, int playerId) {
        UnitStatBarUI statBar = Manager.Instance.GetBehaviour<UnitStatBarUI>(statBarId);
        Player player = Manager.Instance.GetBehaviour<Player>(playerId);
        statBar.username.text = player.photonView.owner.NickName;
        statBar.character = player;
        statBar.transform.SetParent(transform);
        statBar.healthBar.GetComponent<Renderer>().material.color = player.positionUI.color;
        unitBars.Add(statBar);
    }

    public void AssignUnit(MilitaryUnit unit)
    {
        for (int i = 0; i < base.transform.childCount; i++)
        {
            Destroy(base.transform.GetChild(i).gameObject);
        }
        unitBars = new List<UnitStatBarUI>();
                
        foreach (Soldier soldier in unit.soldiers)
        {
            UnitStatBarUI statBar = Instantiate(unitBarPrefab, transform.position, transform.rotation).GetComponent<UnitStatBarUI>();
            statBar.character = soldier;
            statBar.transform.SetParent(transform);
            statBar.healthBar.GetComponent<Renderer>().material.color = unit.color;
            unitBars.Add(statBar);
        }

        UpdateBars();
    }

    void UnitBars()
    {
        Vector3 position = transform.position;

        for (int i = 0; i < unitBars.Count; i++)
        {
            UnitStatBarUI statBar = unitBars[i];
            if (statBar == null)
            {
                unitBars.Remove(statBar);
                UpdateBars();
                return;
            }            
        }
    }

    void UpdateBars()
    {
        Vector3 position = transform.position;

        foreach (UnitStatBarUI statBar in unitBars)
        {            
            statBar.transform.position = position;
            position += (transform.rotation * (Vector3.down * 0.05f));
        }
    }
}
