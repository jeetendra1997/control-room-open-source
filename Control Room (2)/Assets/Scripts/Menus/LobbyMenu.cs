﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyMenu : Singleton<LobbyMenu> {
    
    public GameObject roomBarPrefab;
    public float barOffset;    
    public Transform roomBarPosition;
    public RoomMenu roomMenu;
    public PasswordMenu passwordMenu;
    public RoomBar activeRoomBar;
    public List<RoomBar> roomList;
    public Text errorText;
    public Scrollbar scrollbar;
    public int roomCount;
    public Vector3 initialPosition;

    private void Start()
    {
        initialPosition = roomBarPosition.position;
    }

    private void OnEnable()
    {
        scrollbar.gameObject.SetActive(false);
        StartCoroutine(GenerateListDelayed());
    }

    IEnumerator GenerateListDelayed()
    {
        //yield return new WaitForSeconds(0.5f);
        float delay = 5;
        while (!PhotonNetwork.insideLobby && delay > 0)
        {            
            delay -= Time.deltaTime;
            yield return null;
        }
        GenerateList();
    }

    RoomInfo[] roomInfos;
    public void GenerateList()
    {
        Debug.Log("Generate List");
        PhotonNetwork.JoinLobby();
        roomInfos = PhotonNetwork.GetRoomList();

        for (int i = 0; i < roomList.Count; i++)
        {
            Destroy(roomList[i].gameObject);
        }

        roomList.Clear();


        for (int i = 0; i < roomInfos.Length; i++)
        {
            RoomBar roomBar = Instantiate(roomBarPrefab, roomBarPosition.position, transform.rotation, roomBarPosition).GetComponent<RoomBar>();
            roomBar.GetComponent<RectTransform>().anchoredPosition += (Vector2.down * barOffset * i);
            roomBar.AssignValues(i, roomInfos[i]);
            roomBar.GetComponent<Button>().onClick.AddListener(delegate { SelectRoom(roomBar); });
            roomList.Add(roomBar);
        }

        roomCount = roomInfos.Length;
                
        if (roomCount == 12) roomCount++;
        roomCount -= 11;
        if (roomCount > 0)
        {
            scrollbar.gameObject.SetActive(true);
            scrollbar.size = 1f / roomCount;
        }
        else                    
            scrollbar.size = 1;        
    }    

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return))
            JoinRoom();
        if (Input.GetKeyDown(KeyCode.F5))
            GenerateList();
        Scroll();        
    }

    void Scroll()
    {
        Vector3 newPosition = initialPosition;
        newPosition.y += scrollbar.value * (roomCount * barOffset);
        roomBarPosition.position = newPosition;
    }

    public void SelectRoom(RoomBar roomBar)
    {
        activeRoomBar = roomBar;
    }

    public void JoinRoom()
    {
        if (!activeRoomBar) return;
        RoomInfo roomInfo = MultiplayerManager.GetRoom(activeRoomBar.roomNameText.text);

        if ((bool)MultiplayerManager.GetCustomProperty(roomInfo, CustomPropertyType.Starting))
        {
            errorText.text = "Room is no longer available. Game in progress.";
            return;
        }

        if ((bool)MultiplayerManager.GetCustomProperty(roomInfo, CustomPropertyType.InGame))
        {
            errorText.text = "Room is no longer available. Game in progress.";
            return;
        }

        if (roomInfo.PlayerCount >= roomInfo.MaxPlayers)
        {
            errorText.text = "Room is full.";
            return;
        }        

        Debug.Log(activeRoomBar.roomNameText.text);
        if (roomInfo == null) {
            errorText.text = "Room no longer exists.";
            Debug.Log("no room");
            return;
        }
        if (MultiplayerManager.RoomIsPasswordProtected(roomInfo))
        {
            passwordMenu.roomInfo = roomInfo;
            MenuManager.Instance.OpenMenu(passwordMenu.gameObject);
        }
        else
        {            
            PhotonNetwork.JoinRoom(activeRoomBar.roomNameText.text);
            MenuManager.Instance.OpenMenu(roomMenu.gameObject);
        }
    }    
}
