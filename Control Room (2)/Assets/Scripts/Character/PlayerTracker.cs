﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTracker : CharacterTracker {

    Player player;
    Quaternion cameraRotation;
    Quaternion currentSpineRotation;
    Camera cam;
    public Transform spine;
    public Transform camHandPosition;
    public Transform newHandPosition;
    public bool handSet;

    private void Start()
    {        
        cam = GetComponentInChildren<Camera>(true);
        currentSpineRotation = spine.transform.rotation;
        SetCharacter();
        player = character.GetComponent<Player>();        
    }

    void LateUpdate()
    {
        if (!photonView.isMine && PhotonNetwork.inRoom) {
            //camHandPosition.position = newHandPosition.position;
            //camHandPosition.rotation = newHandPosition.rotation;
            camHandPosition.localPosition = Vector3.zero;
            camHandPosition.localEulerAngles = Vector3.zero;
            camHandPosition.transform.SetParent(newHandPosition);
        }        

        if (photonView.isMine) return;        
        RotateSpine();
        Position();        
    }

    void RotateSpine()
    {
        if (PauseMenu.paused) return;
        if (!PhotonNetwork.inRoom) cameraRotation = cam.transform.rotation;
        Vector3 euler = spine.rotation.eulerAngles;
        Vector3 camEuler = cameraRotation.eulerAngles;
        euler.z -= camEuler.x;
        currentSpineRotation = Quaternion.Lerp(currentSpineRotation, Quaternion.Euler(euler), 0.4f);
        spine.transform.rotation = currentSpineRotation;
        //player.camPivot.transform.rotation = cameraRotation;
        cameraRotation.y = player.transform.rotation.y;
        player.camPivot.transform.rotation = Quaternion.Lerp(player.camPivot.transform.rotation, cameraRotation, 0.4f);
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        SyncCharacter(stream, info);
        SyncCamera(stream, info);
        SyncItemType(stream, info);
        SyncAim(stream, info);
    }

    void SyncAim(PhotonStream stream, PhotonMessageInfo info) {
        if (stream.isWriting)
            stream.SendNext(character.animator.aiming.state);
        else
            character.animator.aiming.state = (bool)stream.ReceiveNext();
    }

    void SyncItemType(PhotonStream stream, PhotonMessageInfo info) {
        if (!PhotonNetwork.inRoom) return;
        if (stream.isWriting) 
        {
            stream.SendNext(character.animator.itemType.value);
        }
        else 
        {
            character.animator.itemType.value = (int)stream.ReceiveNext();
        }        
    }

    void SyncCamera(PhotonStream stream, PhotonMessageInfo info)
    {
        if (!PhotonNetwork.inRoom) return;
        if (stream.isWriting)
        {            
            stream.SendNext(cam.transform.rotation);                        
        }
        else
        {            
            cameraRotation = (Quaternion)stream.ReceiveNext();         
        }
    }
}
