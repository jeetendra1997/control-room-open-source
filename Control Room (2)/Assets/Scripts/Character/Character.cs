﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CharacterDef
{
    public CharacterType type;
    public float health;
    public float damage;
}

public class Character : MultiplayerBehaviour {

    public bool isPlayer;
    public bool localDamage;
    public bool addToRagdollCount;
    public CharacterAnimator animator = new CharacterAnimator();
    public LayerMask groundLayerMask;
    public LayerMask sightLayerMask;
    new public Rigidbody rigidbody;    
    public Item heldItem;
    public List<Limb> limbs = new List<Limb>();
    public Transform hand;
    public PFPath mainPath;
    public PFPath activePath;
    public PF_Node lastNode;
    public Detector detector;
    public Character activeTarget;
    public float health;
    [HideInInspector]
    public float maxHealth;
    public float movementSpeed;
    public float currentMovementSpeed;
    public bool dead;
    public bool startDead;
    public AudioSource audioSource;
    public float detectionDelay;
    public int setDetectionDelay = 30;    
    public Inventory inventory;
    float jumpDelay;

    public Material[] altMaterials;
    public Renderer[] applyMaterials;

    public CameraInterface mountedCam;

    public bool reloading;
    public Ammo[] ammos = new Ammo[4] { new Ammo() { amount = 0, type = GunType.MachineGun }, new Ammo() { amount = 0, type = GunType.Pistol }, new Ammo() { amount = 0, type = GunType.Shotgun }, new Ammo() { amount = 0, type = GunType.Extractor } };

    private void Awake()
    {
        ApplyMaterials();

        isPlayer = GetComponent<Player>();
        SetCharacter();
        RegisterID();
    }

    void ApplyMaterials() {
        if (GlobalSettings.characterQuality == GlobalSettings.QualityLevel.Optimized) {
            if (altMaterials != null && altMaterials.Length > 0) {
                for (int i = 0; i < altMaterials.Length; i++) {
                    applyMaterials[i].material = altMaterials[i];
                }
            }
        }
    }

    public void SetCharacter()
    {
        mountedCam = GetComponentInChildren<CameraInterface>();
        GlobalManager.RegisterCharacter(this);
        maxHealth = health;
        detectionDelay = Random.Range(0, setDetectionDelay);
        detector = GetComponentInChildren<Detector>();
        rigidbody = GetComponent<Rigidbody>();
        if (GetComponentInChildren<Animator>())
            animator.SetAnimator(this);        
        foreach (Limb limb in GetComponentsInChildren<Limb>())
        {
            limbs.Add(limb);
            limb.SetCharacter(this);
            limb.SaveJoint();
        }
        if (rigidbody)
        Ragdoll(false);
    }

    public virtual void UpdateItem() {
        if (heldItem) {
            heldItem.transform.localPosition = Vector3.zero;
            if (((PhotonNetwork.inRoom && photonView.isMine)|| !PhotonNetwork.inRoom )&& reloading && isPlayer)
                heldItem.transform.localEulerAngles = Vector3.Lerp(heldItem.transform.localEulerAngles, Vector3.right * 90, 0.2f);
            else
                heldItem.transform.localEulerAngles = Vector3.Lerp(heldItem.transform.localEulerAngles, Vector3.zero, 0.8f);
        }
    }

    public void UpdateCharacter() {
        animator.Update();
        if (heldItem) {
            animator.itemType.value = (int)heldItem.itemType;            
        } else
            animator.itemType.value = 0;

        if (animator.movement.value.magnitude <= 0)
            currentMovementSpeed = 0;        
        if (jumpDelay > 0)
            jumpDelay -= Time.deltaTime;
    }

    public Limb GetLimb(int id)
    {
        return limbs[id - 1];
    }

    public Ammo GetAmmo(GunType type)
    {
        foreach(Ammo ammo in ammos)
        {
            if (ammo.type == type)
                return ammo;
        }
        return null;
    }

    public void Ragdoll(bool state)
    {
        rigidbody.isKinematic = state;
        GetComponent<Collider>().enabled = !state;
        foreach (Limb limb in limbs) 
        {
            if (state)
            {
                limb.StartCoroutine(limb.Disable(5));
                limb.gameObject.layer = LayerMask.NameToLayer("Ragdoll");
                limb.CreateJoint();
                //if (!limb.rigidbody)
                {
                    //limb.rigidbody = limb.gameObject.AddComponent<Rigidbody>();
                    //limb.rigidbody.mass = 2;
                }
            }
            else
            {
                limb.DestroyJoint();
                //if (limb.rigidbody) Destroy(limb.rigidbody);
            }
            limb.GetComponent<Collider>().isTrigger = !state;
            //limb.rigidbody.isKinematic = !state;
            //limb.rigidbody.GetComponent<Collider>().isTrigger = !state;
        }
        if (animator.animator)
        animator.animator.enabled = !state;
    }

    public void Damage(DamageInfo info)
    {
        if (dead) return;
        if (info.attacker.isPlayer)
            Manager.CreateAudio(transform.position, GlobalManager.GameDef.hitClip, Vector2.zero, 0.1f);
        if (PhotonNetwork.inRoom && !photonView.isMine && localDamage) return;
        int attackerId = 0;
        if (info.attacker) attackerId = info.attacker.id;
        if (PhotonNetwork.inRoom)
        {
            photonView.RPC("RPCDamage", PhotonTargets.AllBuffered, attackerId, info.damage, info.direction, info.limb.id);
            if (dead)            
                photonView.RPC("RPCDie", PhotonTargets.AllBuffered, attackerId, info.damage, info.direction, info.limb.id);            
        }
        else
            RPCDamage(attackerId, info.damage, info.direction, info.limb.id);                        
    }

    DamageInfo damageInfo = new DamageInfo();

    [PunRPC]
    public virtual void RPCDamage(int attackerId, float damage, Vector3 direction, int limbId)
    {        
        //Rigidbody rb = info.limb.GetComponent<Rigidbody>();        
        Character attacker = Manager.Instance.GetBehaviour<Character>(attackerId);
        Limb limb = GetLimb(limbId);
        Rigidbody rb = limb.GetComponent<Rigidbody>();
        if (rb)
        rb.AddForce(direction * (damage / 5), ForceMode.Impulse);
        if (dead) return;

        if (attacker)
            activeTarget = attacker;
        health -= damage;
        if (health <= 0)
        {
            damageInfo.Assign(damage, direction, attacker, limb);
            Die(damageInfo);
        }        
    }

    public void Die(DamageInfo info)
    {
        //if (PhotonNetwork.inRoom)
        //photonView.RPC("RPCDie", PhotonTargets.AllBuffered, info.attacker.id, info.damage, info.direction, info.limb.id);
        //else
        int id = 0;
        if (info.attacker) id = info.attacker.id;
        RPCDie(id, info.damage, info.direction, info.limb.id);
    }

    [PunRPC]
    public virtual void RPCDie(int attackerId, float damage, Vector3 direction, int limbId)
    {        
        if (dead) return;

        if (inventory != null)
        {
            foreach (InventorySlot slot in inventory.slots)
            {
                Equip(slot.item, false);
                Drop(slot.item, false);
            }
        }
        else
            Drop(heldItem, false);

        Manager.Instance.multiplayerBehaviours.Remove(this);        
        DamageInfo info = DamageInfo.GetInfo(this, attackerId, damage, direction, limbId);        
        GlobalManager.RegisterDeath(this);        
        dead = true;        
        health = 0;        
        Ragdoll(true);        
        info.limb.rigidbody.AddForce(info.direction * info.damage / 2, ForceMode.Impulse);        
        animator.animator.transform.parent = null;
        Transform pelvis = animator.animator.transform.Find("Joints").Find("root").Find("pelvis");
        if (addToRagdollCount)
        Manager.AddRagdoll(pelvis.gameObject);
        pelvis.parent = null;
        animator.animator.transform.parent = pelvis;
        Debug.Log(gameObject.name + " has died");
        Destroy(gameObject);        
    }

    public void Equip(Item item, bool RPC)
    {
        CancelReload();
        int id = -1;
        if (item) id = item.id;
        if (PhotonNetwork.inRoom && RPC)
            photonView.RPC("RPCEquip", PhotonTargets.AllBuffered, id);
        else
            RPCEquip(id);
    }

    [PunRPC]
    public void RPCEquip(int itemId)
    {
        Item item = null;
        if (itemId >= 0)
        item = Manager.Instance.GetBehaviour<Item>(itemId);
        //item.SetHolder(this);
        if (heldItem)
        {
            Unequip(heldItem, false);
            heldItem = null;
        }
        if (item == null) {            
            return;
        }
        
        item.gameObject.SetActive(true);
        item.transform.SetParent(hand.transform);
        item.transform.localPosition = Vector3.zero;
        item.transform.localEulerAngles = Vector3.zero;
        heldItem = item;
        inventory.SetActiveItem(item);
    }

    public void Unequip(Item item, bool RPC)
    {
        CancelReload();
        int id = -1;
        if (item) id = item.id;
        if (PhotonNetwork.inRoom && RPC)
            photonView.RPC("RPCUnequip", PhotonTargets.AllBuffered, id);
        else
            RPCUnequip(id);
    }

    [PunRPC]
    public void RPCUnequip(int itemId)
    {
        Item item = Manager.Instance.GetBehaviour<Item>(itemId);
        if (item == heldItem) heldItem = null;        
        item.gameObject.SetActive(false);
        inventory.SetActiveItem(null);
    }

    public void PickUp(Item item)
    {
        //if (PhotonNetwork.inRoom)
            //photonView.RPC("RPCPickUp", PhotonTargets.AllBuffered, item.id);
        //else
            RPCPickUp(item.id);
    }

    [PunRPC]
    public void RPCPickUp(int itemId)
    {
        Item item = Manager.Instance.GetBehaviour<Item>(itemId);        
        if (!inventory.CanDeposit(item)) return;
        inventory.Deposit(item);
        Equip(item, false);
        //if (heldItem) Drop(heldItem);
        item.SetHolder(this);
        //heldItem = item;
        item.transform.SetParent(hand);
        item.transform.position = hand.transform.position;
        item.transform.rotation = hand.transform.rotation;
        //item.transform.localPosition = Vector3.zero;
        //item.transform.eulerAngles = Vector3.zero;
    }

    public void Drop(Item item, bool RPC = true)
    {
        CancelReload();
        int id = -1;
        if (item != null) id = item.id;
        if (PhotonNetwork.inRoom && RPC)
            photonView.RPC("RPCDrop", PhotonTargets.AllBuffered, id);
        else
            RPCDrop(id);
    }

    [PunRPC]
    public void RPCDrop(int itemId)
    {
        Item item = null;
        if (itemId > -1)
            item = Manager.Instance.GetBehaviour<Item>(itemId);
        if (item == heldItem)
            heldItem = null;
        if (item == null || !item.GetComponent<Rigidbody>()) return;
        inventory.Drop(item);
        item.Drop(false);
        item.transform.position = transform.position + transform.forward + Vector3.up;
        item.GetComponent<Rigidbody>().AddForce(transform.forward, ForceMode.Impulse);
    }
    
    public void Jump(Vector3 direction)
    {
        
        if (jumpDelay > 0) return;

        if (Physics.CheckSphere(transform.position, 0.2f, groundLayerMask))
            rigidbody.AddForce(direction, ForceMode.Impulse);
    }
    
    public bool FollowPath(PFPath path, float speed, bool lookDirection)
    {
        if (path.nodes.Count == 0) return false;
        Vector3 destination = path.nodes[0].transform.position;
        destination.y = transform.position.y;
        Vector3 direction = destination - transform.position;
        if (direction.magnitude < 1)
        {
            lastNode = path.nodes[0];
            path.nodes.Remove(path.nodes[0]);
        }
        Move((direction).normalized, speed, lookDirection, true);
        return true;
    }

    public void Move(Vector3 direction, float speed, bool lookDirection, bool normalizeDirection, bool instant = false)
    {
        direction.y = 0;
        if (animator.animator != null)
        animator.Move(direction, speed);

        if (normalizeDirection) direction = direction.normalized;

        if (instant)
            currentMovementSpeed = speed;
        else
            currentMovementSpeed = Mathf.Lerp(currentMovementSpeed, speed, 0.1f);
        

        rigidbody.position += (direction * currentMovementSpeed) * (Time.deltaTime * 30);
        
        if (lookDirection)
        {
            if (direction != Vector3.zero)
                rigidbody.rotation = Quaternion.Lerp(rigidbody.rotation, Quaternion.LookRotation(direction), speed * 2);
            else rigidbody.angularVelocity = Vector3.zero;
        }
        else
        {
            rigidbody.angularVelocity = Vector3.zero;
        }
    }
    
    public void LookAt(Vector3 position)
    {
        LookTowards(position - transform.position);
    }

    public void LookTowards(Vector3 direction, float speed = 0.1f)
    {
        direction.y = 0;
        rigidbody.rotation = Quaternion.Lerp(rigidbody.rotation, Quaternion.LookRotation(direction), speed);
    }

    public void AimAt(Vector3 position, float speed = 0.1f)
    {
        Aim(position - transform.position, speed);
    }

    public void Aim(Vector3 direction, float speed = 0.1f)
    {
        LookTowards(direction, speed);
        animator.aiming.state = true;
    }

    public void Attack()
    {
        animator.attacking.state = true;
    }

    public void Reload()
    {
        if (reloading) return;
        animator.reloading.Trigger();
    }

    public void CancelReload()
    {
        animator.cancelReload.state = true;
        reloading = false;
    }

    public float DistancedMovementSpeed(Vector3 current, Vector3 end, float speedCap, float speedMultiplier)
    {
        return Mathf.Clamp(((end - current).magnitude * 0.02f * speedMultiplier), 0, speedCap);
    }

    public float DistancedMovementSpeed(Vector3 current, Vector3 end, float speedCap)
    {
        return DistancedMovementSpeed(current, end, speedCap, 1);
    }

}
