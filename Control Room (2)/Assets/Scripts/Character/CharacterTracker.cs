﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterTracker : MultiplayerBehaviour {

    public Character character;
    Vector3 activePosition;
    Vector3 previousPosition;
    Quaternion activeRotation;


    public Transform changeLayer;
    public GameObject[] disableOnStart;
	
	void Awake () {
        RegisterID();
        SetCharacter();
	}

    public void SetCharacter()
    {
        character = GetComponent<Character>();
        if (!photonView.isMine && PhotonNetwork.inRoom)
        {
            character.rigidbody.isKinematic = true;
            if (changeLayer)
            {
                foreach (Transform child in changeLayer.GetComponentsInChildren<Transform>())
                    child.gameObject.layer = 0;
            }
            foreach (GameObject disable in disableOnStart)
                disable.SetActive(false);
        }
    }
	
	void LateUpdate () {
        if (photonView.isMine) return;
        Position();
	}

    public void Position()
    {
        if (!PhotonNetwork.inRoom) return;
        //Vector3 velocity = ((activePosition - previousPosition) * 60) * Time.deltaTime;
        Vector3 velocity = activePosition - previousPosition;
        character.rigidbody.position = Vector3.Lerp(transform.position, activePosition + velocity, 0.15f);
        character.rigidbody.rotation = Quaternion.Lerp(transform.rotation, activeRotation, 0.4f);
    }    
   

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        SyncCharacter(stream, info);
    }

    public void SyncCharacter(PhotonStream stream, PhotonMessageInfo info) {
        previousPosition = activePosition;

        if (!character || character.animator == null)
            return;

        if (!PhotonNetwork.inRoom) return;
        if (stream.isWriting) {
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
            stream.SendNext(character.animator.attacking.state);
            stream.SendNext(character.animator.movement.value);
            stream.SendNext(character.animator.movementSpeed.value);
            stream.SendNext(character.health);

        } else {
            activePosition = (Vector3)stream.ReceiveNext();
            activeRotation = (Quaternion)stream.ReceiveNext();
            character.animator.attacking.state = (bool)stream.ReceiveNext();
            character.animator.movement.value = Vector2.Lerp(character.animator.movement.value, (Vector2)stream.ReceiveNext(), 0.3f);
            character.animator.movementSpeed.value = Mathf.Lerp(character.animator.movementSpeed.value, (float)stream.ReceiveNext(), 0.3f);
            character.health = (float)stream.ReceiveNext();

            if (character.animator.movement.value.magnitude <= 0.2f)
                character.currentMovementSpeed = 0;            
        }
    }
}
