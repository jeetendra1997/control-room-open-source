﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour {

    public Vector3 rotate;
	
	void Start () {
        Vector3 euler = transform.eulerAngles;
        euler += rotate;
        transform.eulerAngles = euler;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
