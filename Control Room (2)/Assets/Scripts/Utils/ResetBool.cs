﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetBool
{
    public int timer;
    bool _state;
    public bool state { get { return _state; } set { _state = value; timer = 0; } }

    public void Update()
    {
        if (timer < 2)
        {
            timer++;
            if (timer > 1)
            {
                _state = false;
            }
        }
    }

    public static implicit operator bool(ResetBool resetBool)
    {
        return resetBool.state;
    }
}