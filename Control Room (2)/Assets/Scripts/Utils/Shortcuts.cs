﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Shortcuts : MonoBehaviour {
#if UNITY_EDITOR
    [MenuItem("Shortcut/ClearPlayerPrefs")]
    static void ClearPlayerPrefs() {
        PlayerPrefs.DeleteAll();
    }

    [MenuItem("Shortcut/SnapPosition %#w")]
    static void SnapPosition()
    {
        Transform transform = Selection.activeTransform;
        if (!transform) return;
        transform.position = new Vector3(Mathf.Round(transform.position.x), Mathf.Round(transform.position.y), Mathf.Round(transform.position.z));
    }

    [MenuItem("Shortcut/CreateChild %#d")]
    static void CreateChild()
    {
        GameObject gameObject = Selection.activeGameObject;
        if (!gameObject) return;
        string name = gameObject.name;
        string number = "";
        foreach(char character in name.ToCharArray())
        {
            bool isNumber = false;
            for (int i = 0; i < 10; i++)
            {
                if (character == i.ToString().ToCharArray()[0])
                {
                    number += character.ToString();
                    isNumber = true;
                }                
            }
            if (!isNumber) number = "";
        }     
        if (number == "")
            name += 1;
        else
        {         
            name = name.Replace(number, "");         
            name += int.Parse(number) + 1;
        }
        GameObject child = new GameObject(name);
        child.transform.position = gameObject.transform.position;
        child.transform.rotation = gameObject.transform.rotation;
        child.transform.SetParent(gameObject.transform);

        Selection.SetActiveObjectWithContext(child, null);
    }
#endif
}
