﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Ingredient
{

    public enum Type
    {
        Red,
        Blue,
        Green,
        Cyan,
        Magenta,
        Orange,
    }

    public enum Temperature
    {
        None,
        Cold,
        Hot,
    }

    public Type type;
    public Temperature temperature;
    public Color color;

}

public class CureManager : Singleton<CureManager> {

    public Ingredient[] ingredients;

    private void Start()
    {
        if (PhotonNetwork.isMasterClient || !PhotonNetwork.inRoom)
        {
            AssignBeakers();
        }
    }

    void AssignBeakers()
    {
        Beaker[] beakers = GameObject.FindObjectsOfType<Beaker>();
        if (beakers.Length < 1) return;
        List<Beaker> addBeakers = new List<Beaker>();

        foreach(Beaker beaker in beakers)
        {
            if (beaker.disclude)
                GenerateBeaker(beaker, beaker.ingredient.type);
        }

        int attempt = 1000;
        while (addBeakers.Count < 12 && attempt > 0)
        {
            Beaker beaker = beakers[Random.Range(0, beakers.Length)];
            if (!addBeakers.Contains(beaker) && !beaker.disclude)
                addBeakers.Add(beaker);
            attempt--;            
        }

        for (int i = 0; i < beakers.Length; i++)
        {
            if (!addBeakers.Contains(beakers[i]) && !beakers[i].disclude)
            {
                DestroyItem(beakers[i]);
            }
        }

        int currentIngredient = 0;

        for (int i = 0; i < addBeakers.Count; i++)
        {
            GenerateBeaker(addBeakers[i], (Ingredient.Type)currentIngredient);

            currentIngredient = (currentIngredient + 1) % 3;
        }
    }    

    void DestroyItem(MultiplayerBehaviour item)
    {
        if (PhotonNetwork.inRoom)
            GetComponent<PhotonView>().RPC("RPCDestroyItem", PhotonTargets.AllBuffered, item.id);

        else
            RPCDestroyItem(item.id);
        
    }

    [PunRPC]
    void RPCDestroyItem(int itemId)
    {
        Item item = MultiplayerManager.Instance.GetBehaviour<Item>(itemId);
        string itemString = itemId.ToString(); if (item) itemString = item.name;
        //Debug.Log("Attempting to destroy item " + itemString );
        Destroy(item.gameObject);
    }

    public void GenerateBeaker(Beaker beaker, Ingredient.Type type)
    {
        if (PhotonNetwork.inRoom)
            GetComponent<PhotonView>().RPC("RPCGenerateBeaker", PhotonTargets.AllBuffered, beaker.id, (int)type);
        else
            RPCGenerateBeaker(beaker.id, (int)type);
    }

    [PunRPC]
    public void RPCGenerateBeaker(int beakerId, int typeId)
    {
        Beaker beaker = MultiplayerManager.Instance.GetBehaviour<Beaker>(beakerId);
        Ingredient.Type type = (Ingredient.Type)typeId;
        foreach (Ingredient ingredient in ingredients)
        {
            if (ingredient.type == type)
            {
                beaker.ingredient.type = type;
                beaker.mixture.GetComponent<Renderer>().material.color = ingredient.color;
            }
        }
    }
}
