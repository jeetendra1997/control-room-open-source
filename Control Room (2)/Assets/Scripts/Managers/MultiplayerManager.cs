﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public enum GameState
{
    Waiting,
    Start,
    Playing
}

[RequireComponent(typeof(PhotonView))]
public class MultiplayerManager : Manager {

    static MultiplayerManager _instance;
    new public static MultiplayerManager Instance { get { if (!_instance) _instance = FindObjectOfType<MultiplayerManager>(); return _instance; } }    

    public GameObject exitUI;

    public bool connected;

    public static bool spectatingOutside;
    public static bool spectating;
    public static bool rescueArrived;
    public Transform spectateTransform;
    public int spectateIndex;

    public static Camera cam;
    public static List<PhotonPlayer> playersInGame = new List<PhotonPlayer>();    
    public static Beaker cure;

    public List<Player> players = new List<Player>();

    PhotonView photonView;
    public GameState gameState;

    public int playersConnected;
    public Transform playerSpawnContainer;

    public AudioSource rescueArrivedAudioSource;
    public AudioClip helicopterClip;

    public static Room activeRoom;

    public static bool RoomExists(string name)
    {
        return GetRoom(name) != null;        
    }

    public static RoomInfo GetRoom(string name)
    {
        foreach (RoomInfo room in PhotonNetwork.GetRoomList())
        {         
            if (room.Name == name) return room;
        }
        return null;
    }

    public static bool RoomIsPasswordProtected(RoomInfo room)
    {
        return (bool)room.CustomProperties[GetCustomProperty(CustomPropertyType.PasswordProtected)];
    }

    public static string GetRoomPassword(RoomInfo room)
    {
        return (string)room.CustomProperties[GetCustomProperty(CustomPropertyType.Password)];        
    }

    public List<CustomProperty> customProperties = new List<CustomProperty>();

    public static string GetCustomProperty(CustomPropertyType type)
    {
        return GlobalManager.GameDef.GetCustomProperty(type);
    }

    public static object GetCustomProperty(Room room, CustomPropertyType type)
    {
        return room.CustomProperties[GetCustomProperty(type)];
    }

    public static object GetCustomProperty(RoomInfo room, CustomPropertyType type)
    {
        return room.CustomProperties[GetCustomProperty(type)];
    }

    public static void UpdateCustomProperty(Room room, CustomPropertyType type, object value)
    {
        room.CustomProperties[GetCustomProperty(type)] = value;
        room.SetCustomProperties(room.CustomProperties);
    }

    public static bool CreateRoom(string name, bool passwordProtected, string password, byte maxPlayers) {
        RoomOptions options = new RoomOptions();
        options.MaxPlayers = maxPlayers;
        options.CustomRoomProperties = new ExitGames.Client.Photon.Hashtable();
        options.CustomRoomProperties.Add(GetCustomProperty(CustomPropertyType.PasswordProtected), passwordProtected);
        options.CustomRoomProperties.Add(GetCustomProperty(CustomPropertyType.Password), password);
        options.CustomRoomProperties.Add(GetCustomProperty(CustomPropertyType.Map), GlobalManager.levelInfo.level);
        options.CustomRoomProperties.Add(GetCustomProperty(CustomPropertyType.Difficulty), GlobalManager.levelInfo.difficulty);
        options.CustomRoomProperties.Add(GetCustomProperty(CustomPropertyType.Starting), false);
        options.CustomRoomProperties.Add(GetCustomProperty(CustomPropertyType.InGame), false);

        List<string> list = new List<string>();
        for (int i = 0; i < (int)CustomPropertyType.LENGTH; i++)
        {
            list.Add(GetCustomProperty((CustomPropertyType)i));
        }
        options.CustomRoomPropertiesForLobby = list.ToArray();

                
        if (PhotonNetwork.CreateRoom(name, options, null))
        {                        
            return true;
        }
        else
        {
            return false;
        }
    }   

    private void Awake() {
        Reset();
        SetLights();
        photonView = GetComponent<PhotonView>();
        if (spectatingOutside)
        {
            Player.isDead = true;
            cam = Instantiate(GlobalManager.GameDef.cameraPrefab, Vector3.zero, Quaternion.identity).GetComponent<Camera>();
            PlayAudio(audioSource, ambienceClip);
            LockDownSet();
        }
        else
        {
            SpawnPlayer();
            cam = Player.Instance.cam;
            spectateTransform = Player.Instance.camPivot.transform;            
        }        
                
        if (PhotonNetwork.inRoom) {
            photonView.RPC("RPCConnectToMap", PhotonTargets.AllBufferedViaServer);
        }        
    }

    private void Update() {
        DebugMode();
        DetectionManager.Update();        
        CheckDisconnect();
        Spectating();
        Pausing();        
        GlobalManager.totalTime += Time.deltaTime;
        if (gameState == GameState.Playing) {
            Play();
        }
        if (!PhotonNetwork.isMasterClient) return;
        if (gameState == GameState.Waiting) {
            WaitForPlayers();
        } else {
            GamePlay();
        }

        Transmissions();        
    }


    public static void SetupRoom() {
        spectating = false;
        rescueArrived = false;
        GlobalManager.hasCure = false;
        GlobalManager.cureGenerated = false;
        GlobalManager.survived = false;
        //GlobalManager.levelInfo = new LevelInfo() { difficulty = 0, level = Level.Factor };        

        playersInGame.Clear();
        foreach (PhotonPlayer player in PhotonNetwork.playerList) {
            playersInGame.Add(player);
        }

        Analytics.CustomEvent("StartOnlineMatch", new Dictionary<string, object>
            {
                { "playerID", GlobalManager.playerID }
            });
    }

    List<Transform> playerSpawnPoints = new List<Transform>();
    void SpawnPlayer() {
        playerSpawnPoints.Clear();
        for (int i = 0; i < playerSpawnContainer.childCount; i++) {
            playerSpawnPoints.Add(playerSpawnContainer.GetChild(i));
        }
        int random = Random.Range(0, playerSpawnPoints.Count);
        Vector3 offset = new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f));
        if (PhotonNetwork.inRoom)
            PhotonNetwork.Instantiate(playerPrefab.gameObject.name, playerSpawnPoints[random].position + offset, playerSpawnPoints[random].rotation, 0);
        else {
            Instantiate(playerPrefab, playerSpawnPoints[random].position + offset, playerSpawnPoints[random].rotation);
        }
    }

    public override void RemovePlayer(Player player, bool survived, bool hasCure) {
        PhotonPlayer photonPlayer = player.photonView.owner;
        Debug.Log("Remove " + photonPlayer.NickName);

        if (!GlobalManager.survived)
            GlobalManager.survived = survived;

        if (!GlobalManager.hasCure)
            GlobalManager.hasCure = hasCure;

        if (survived)
            GlobalManager.playersSurvived++;

        //if (hasCure) 
        //GlobalManager.levelInfo.difficulty = 1;        

        Debug.Log(GlobalManager.levelCompleted);

        if (player) {
            ChangeTrackerLayer(player.camPivot.transform, 0);
            if (survived) {                
                if (photonPlayer.IsLocal)
                {
                    Player.isDead = true;
                    spectating = true;
                    cam.transform.SetParent(null);
                }
                Destroy(player.gameObject);                
            } else
                Destroy(player);
        }

        playersInScene = GameObject.FindObjectsOfType<Player>();

        if (playersInScene.Length <= 1)
        {
            //LOOK INTO THIS SHIT FOR END OF ROUND LEAVING important

            //if (GlobalManager.survived)
            //GlobalManager.Instance.LevelComplete(GlobalManager.levelInfo.level);
            //GlobalManager.levelCompleted = true;
            StartCoroutine(DelayedQuit());
            //Quit(true, GlobalManager.survived);
            //GlobalManager.Instance.LoadImmediate(0);
        }
    }

    IEnumerator DelayedQuit() {
        WaitForSeconds wait = new WaitForSeconds(0.5f);
        yield return wait;
        Debug.Log(GlobalManager.levelCompleted);
        //if (spectatingOutside)
        //Quit(true, false);
        //else
        StartCoroutine(ReturnToMenu());
    }

    public void Spectate(Transform newTransform) {
        if (spectateTransform) ChangeTrackerLayer(spectateTransform, 0);
        spectateTransform = newTransform;
        ChangeTrackerLayer(spectateTransform, 5);
        spectating = true;
    }

    public void ChangeTrackerLayer(Transform transform, int layer) {        
        if (transform != null) {
            CharacterTracker tracker = transform.GetComponentInParent<CharacterTracker>();
            if (tracker) {                
                if (tracker.changeLayer) {
                    foreach (Transform child in tracker.changeLayer.GetComponentsInChildren<Transform>())
                        child.gameObject.layer = layer;
                }
            }
        }
    }

    Player[] playersInScene;
    void Spectating() {
        if (!spectating) return;

        if (playersInScene.Length < 1)
        {
            StartCoroutine(ReturnToMenu());            
            return;
        }

        if (Input.GetMouseButtonDown(0)) {
            playersInScene = GameObject.FindObjectsOfType<Player>();            
            spectateIndex = (spectateIndex+1) % playersInScene.Length;
            Spectate(playersInScene[spectateIndex].camPivot.transform);
            string objective = playersInScene[spectateIndex].photonView.owner.NickName+"\n";
            objective += ObjectiveUI.UpdateText();
            ObjectiveUI.AssignText(objective);
        }
        if (Input.GetMouseButtonDown(1)) {
            playersInScene = GameObject.FindObjectsOfType<Player>();
            spectateIndex = (spectateIndex - 1) % playersInScene.Length;
            Spectate(playersInScene[spectateIndex].camPivot.transform);
            string objective = playersInScene[spectateIndex].photonView.owner.NickName + "\n";
            objective += ObjectiveUI.UpdateText();
            ObjectiveUI.AssignText(objective);
        }

        if (!spectateTransform) {
            playersInScene = GameObject.FindObjectsOfType<Player>();            
            spectateIndex = (spectateIndex + 1) % playersInScene.Length;
            Spectate(playersInScene[spectateIndex].camPivot.transform);
        }
        cam.transform.position = spectateTransform.position;
        cam.transform.rotation = spectateTransform.rotation;
    }    

    IEnumerator ReturnToMenu()
    {
        spectating = false;

        GameObject loadingScreenPrefab = Resources.Load<GameObject>("LoadingScreen");
        Instantiate(loadingScreenPrefab, FindObjectOfType<Canvas>().transform);
        FindObjectOfType<AudioListener>().transform.position = Vector3.down * 1000f;
        yield return null;

        if (PhotonNetwork.inRoom)
            PhotonNetwork.LeaveRoom();

        Quit(true, GlobalManager.survived);
    }

    void CheckDisconnect() {
        if (connected && (!PhotonNetwork.inRoom || !PhotonNetwork.connectedAndReady)) {
            if (!disconnectDelay)
            {
                StartCoroutine(DelayDisconnect());
            }
        }
    }    

    bool disconnectDelay;

    IEnumerator DelayDisconnect()
    {
        float delay = 5;
        bool disconnect = true;
        while (delay > 0)
        {
            delay -= Time.deltaTime;
            if (connected && PhotonNetwork.inRoom && PhotonNetwork.connectedAndReady)
            {
                delay = 0;
                disconnect = false;                
            }
            yield return null;
        }
        if (disconnect)
            Quit(false, false);
        //GlobalManager.Instance.LoadImmediate(0);
    }

    public override IEnumerator PlayerDeath(int id)
    {
        Player player = GetBehaviour<Player>(id);
        PhotonPlayer photonPlayer = player.photonView.owner;
        Debug.Log(photonPlayer.NickName + " has died");

        RemovePlayer(player, false, false);

        yield return new WaitForSeconds(1);
        if (photonPlayer.IsLocal)
            spectating = true;
    }

    public void GamePlay() {
        if (gameState == GameState.Start && !startLockDown) {            
            UpdateCustomProperty(PhotonNetwork.room, CustomPropertyType.Starting, false);
            UpdateCustomProperty(PhotonNetwork.room, CustomPropertyType.InGame, true);

            startLockDown = true;
            photonView.RPC("RPCStartLockDown", PhotonTargets.AllViaServer);
        }
    }

    void Play() {
        if (!rescueAudioCreated && timeRemaining < 5) {
            GameObject rescueAudioGO = CreateAudio(transform.position, rescueClip, Vector2.zero, 0.1f).gameObject;            
            PlayAudio(rescueArrivedAudioSource, helicopterClip);
            StartCoroutine(ExitUI());
            rescueArrived = true;
            rescueAudioCreated = true;
            ObjectiveUI.UpdateText();
        }
        if (!CRInterface.Instance.transmissionDown)
        {
            timeRemaining -= Time.deltaTime;
            //if ((int)timeRemaining % 5 == 1)
                //photonView.RPC("RPCUpdateTimeRemaining", PhotonTargets.AllBufferedViaServer, timeRemaining);

        }
    }
    [PunRPC]
    public void RPCUpdateTimeRemaining(float timeRemaining)
    {
        if (spectatingOutside)
        MultiplayerManager.timeRemaining = timeRemaining;
    }

    IEnumerator ExitUI() {
        while (rescueArrivedAudioSource.isPlaying) {            
            exitUI.SetActive(true);
            yield return new WaitForSeconds(1);
            exitUI.SetActive(false);
            yield return new WaitForSeconds(1);
        }
    }


    [PunRPC]
    public void RPCStartLockDown()
    {        
        if (!lockDown)
            StartCoroutine("StartLockDown");            
    }

    public override IEnumerator StartLockDown()
    {        
        timeRemaining = 60 * surviveForMinutes;
        foreach (BlastDoor door in GameObject.FindObjectsOfType<BlastDoor>())
        {
            door.OpenDoor(false);
        }
        yield return new WaitForSeconds(3);
        
        CRInterface.Instance.LogText("LOCK DOWN");
        float timer = 0;
        int state = -1;
        GameObject audioSourceGO = CreateAudio(transform.position, lockDownClip, Vector3.one * 0.9f, 0.1f).gameObject;
        AudioSource newAudioSource = audioSourceGO.GetComponent<AudioSource>();
        while (audioSourceGO != null && audioSourceGO.activeSelf)
        {           
            timer += Time.deltaTime * 70;
            if (timer > 100)
            {
                state++;
                timer = 0;
                if (state == 0)
                    CRInterface.Instance.LogText("LOCK DOWN");
                else if (state == 1)
                    CRInterface.Instance.LogText("RESCUE ALERTED");
                else if (state == 2)
                {
                    CRInterface.Instance.LogText("MAINTAIN TRANSMISSION");
                    state = -1;
                }
            }
            yield return null;
        }        
        CreateAudio(transform.position, lightSwitchClip, Vector3.zero, 0.5f);
        PlayAudio(audioSource, ambienceClip);
        LockDownSet();        
        SetGameState(GameState.Playing);        
    }

    public void LockDownSet() {
        lockDown = true;
        ObjectiveUI.UpdateText();
        /*
        if (PhotonNetwork.inRoom) photonView.RPC("RPCLockDownSet", PhotonTargets.AllBuffered);
        else RPCLockDownSet();
        */
    }

    [PunRPC]
    void RPCLockDownSet() {
        Debug.Log("Start lockdown");
        lockDown = true;
    }

    void WaitForPlayers()
    {                
        if (playersConnected >= playersInGame.Count)
        {
            SetGameState(GameState.Start);
        }
    }    

    public void SetGameState(GameState state)
    {
        photonView.RPC("RPCSetGameState", PhotonTargets.AllBuffered, (int)state);
    }

    [PunRPC]
    public void RPCSetGameState(int state)
    {
        gameState = (GameState)state;
    }
   

    [PunRPC]
    public void RPCConnectToMap()
    {
        connected = true;
        if (!spectating)
            playersConnected++;        
    }

    public void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
        if (playersInGame.Contains(player))
            playersInGame.Remove(player);
    }
}
