﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class CharacterAnimator {

    public Character character;
    public Animator animator;    

    public List<AnimatorVariable> animatorVariables = new List<AnimatorVariable>();
    
    public AnimatorVector2 movement;
    public AnimatorFloat movementSpeed;
    public AnimatorInt itemType;
    public AnimatorBool aiming;
    public AnimatorBool attacking;
    public AnimatorBool cancelReload;
    public AnimatorTrigger reloading;

    public void SetAnimator(Character character)
    {
        this.character = character;
        animator = character.GetComponentInChildren<Animator>();
        animatorVariables.Add(movement = new AnimatorVector2(animator, "movement"));
        animatorVariables.Add(movementSpeed = new AnimatorFloat(animator, "movementSpeed"));
        //if (character.isPlayer)
        animatorVariables.Add(itemType = new AnimatorInt(animator, "itemType"));
        animatorVariables.Add(aiming = new AnimatorBool(animator, "aiming"));
        animatorVariables.Add(attacking = new AnimatorBool(animator, "attacking"));
        animatorVariables.Add(cancelReload = new AnimatorBool(animator, "cancelReload"));
        animatorVariables.Add(reloading = new AnimatorTrigger(animator, "reloading"));
    }

    public void Update()
    {
        foreach(AnimatorVariable animVar in animatorVariables)
        {            
            animVar.Update();
        }
    }

    public void Move(Vector3 direction, float speed)
    {
        //direction = character.transform.rotation * direction;        
        direction = Quaternion.Inverse(character.transform.rotation) * direction;

        direction = Vector2.Lerp(movement.value, new Vector2(direction.x, direction.z), 0.1f);

        float runSpeed = Mathf.Clamp(speed * 30, 0.75f, 1);        
        movement.value = new Vector2(direction.x, direction.y) * runSpeed;


        if (speed == 0)
        {
            movementSpeed.value = 0;
        }
        else if (speed < 0.8)
        {
            movementSpeed.value = Mathf.Lerp(movementSpeed.value, Mathf.Clamp(speed, 0.05f, 0.06f), 0.05f);           
        }
        else
        {
            movementSpeed.value = Mathf.Lerp(movementSpeed.value, Mathf.Clamp(speed, 0.07f, 0.08f), 0.05f);        
        }
    }
}
