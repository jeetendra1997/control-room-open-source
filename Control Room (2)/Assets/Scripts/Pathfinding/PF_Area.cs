﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PF_Area : MonoBehaviour {

    public List<PF_Region> regions = new List<PF_Region>();

    public Vector2 areaSize;
    public Vector2 regionSize;
    public Vector2 gridSize;
    public float gridSpacing;
    public bool generateGridOnStart;    

    [HideInInspector]
    public bool neighboursAssigned;
    [HideInInspector]
    public bool gridsBaked;
    [HideInInspector]
    public bool showGizmos;
    


    private void Awake()
    {
        if (generateGridOnStart)
            GenerateGrid();        
        Pathfind.areas.Add(this);
    }

    private void Start()
    {
        if (generateGridOnStart)
        {
            foreach (PF_Region region in regions)
            {
                foreach (PF_Grid grid in region.grids)
                {
                    grid.AssignNeighbours(Pathfind.areas);
                }
            }
        }
    }

    public PF_Node GetNode(Vector3 position, Vector3 nearPosition)
    {                
        float sizeX = areaSize.x * regionSize.x * (gridSize.x );
        float sizeY = areaSize.y * regionSize.x * (gridSize.y );        
        if (position.x > transform.position.x + sizeX / 2 || position.x < transform.position.x - sizeX / 2 || position.z > transform.position.z + sizeY || position.z < transform.position.z - sizeY)
        {            
            return null;
        }        

        foreach (PF_Region region in regions)
        {
            PF_Node node = region.GetNode(position, nearPosition);            
                if (node != null)
                    return node;                            
        }        
        return null;
    }

    public void AssignNeighbours()
    {
        foreach (PF_Region region in regions)
        {
            foreach (PF_Grid grid in region.grids)
            {
                foreach (PF_Node node in grid.nodes)
                    node.neighbours = new List<PF_Node>();
            }
        }
        foreach (PF_Region region in regions)
        {
            foreach (PF_Grid grid in region.grids)
            {
                grid.AssignNeighbours(new List<PF_Area>() { this });
            }            
        }
    }

    public void AssignWeights()
    {
        foreach (PF_Region region in regions)
        {
            foreach (PF_Grid grid in region.grids)
            {
                foreach (PF_Node node in grid.nodes)
                    node.avoidanceWeight = 0;
            }
        }
        foreach (PF_Region region in regions)
        {
            foreach(PF_Grid grid in region.grids)
            {
                grid.AssignWeight();
            }
        }
    }

    public void GenerateGrid()
    {
        for (int i = 0; i < regions.Count; i++)
        {
            DestroyImmediate(regions[i].gameObject);
        }
        regions = new List<PF_Region>();

        if (areaSize.x == 0 || areaSize.y == 0 || regionSize.x == 0 || regionSize.y == 0 || gridSize.x == 0 || gridSize.y == 0) return;

        float xOffset = ((areaSize.x - 1) / 2) * regionSize.x * gridSize.x;
        float yOffset = ((areaSize.y - 1) / 2) * regionSize.y * gridSize.y;
        
        for (float x = -xOffset; x <= xOffset; x += gridSize.x * regionSize.x)        
        {
            for (float y = -yOffset; y <= yOffset; y += gridSize.y * regionSize.y)            
            {
                GameObject regionGO = new GameObject("Region");                
                regionGO.transform.position = new Vector3(x, 0, y) + transform.position;
                regionGO.transform.SetParent(transform);
                PF_Region region = regionGO.AddComponent<PF_Region>();
                region.GenerateGrid(regionSize, gridSize, gridSpacing);
                region.area = this;
                regions.Add(region);
            }
        }
    }

    public void PrepareBaking()
    {
        foreach(PF_Region region in regions)
        {
            foreach(PF_Grid grid in region.grids)
            {
                foreach(PF_Node node in grid.nodes)
                {
                    node.Reset();
                }
            }
        }

        foreach(BlastDoor door in GameObject.FindObjectsOfType<BlastDoor>())
        {
            door.nodes = new List<PF_Node>();
        }        
    } 
}
