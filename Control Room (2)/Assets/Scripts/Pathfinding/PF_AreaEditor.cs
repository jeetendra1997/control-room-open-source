﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(PF_Area))]
[CanEditMultipleObjects]
public class PF_AreaEditor : Editor {

    bool bakeGrids;
    int currentRegion;

    private void OnEnable()
    {
        EditorApplication.update += Bake;        
    }

    private void OnDisable()
    {
        EditorApplication.update -= Bake;        
    }


    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        PF_Area area = (PF_Area)target;

        if (!bakeGrids)
        Buttons(area);
        Warnings(area);        
        
    }

    void Warnings(PF_Area area)
    {
        if (bakeGrids)
            EditorGUILayout.HelpBox("BAKING GRID. PLEASE WAIT", MessageType.Error);

        if (!area.neighboursAssigned)
            EditorGUILayout.HelpBox("Neighbours have not been assigned", MessageType.Warning);
        if (!area.gridsBaked)
            EditorGUILayout.HelpBox("Grids have not been baked", MessageType.Warning);
    }

    void Buttons(PF_Area area)
    {
        if (GUILayout.Button("GenerateGrids"))
        {
            Debug.Log("Grid Generated");

            area.GenerateGrid();
            ShowGizmos(true);
            area.neighboursAssigned = false;
            area.gridsBaked = false;
        }

        if (area.regions.Count < 1) return;
        if (GUILayout.Button("BakeGrids"))
        {
            Debug.Log("Baking grids...");
            area.PrepareBaking();
            currentRegion = 0;
            bakeGrids = true;
            area.gridsBaked = true;
            area.neighboursAssigned = false;            
        }

        if (GUILayout.Button("AssignNeighbours"))
        {
            Debug.Log("Neighbours Assigned");
            area.AssignNeighbours();
            area.neighboursAssigned = true;
        }

        if (area.gridsBaked && area.neighboursAssigned)
        {
            if (GUILayout.Button("AssignWeights"))
            {
                Debug.Log("Weights Assigned");
                area.AssignWeights();
                SceneView.RepaintAll();
            }
        }

        if (!area.showGizmos)
        {
            if (GUILayout.Button("ShowGizmos"))
            {
                ShowGizmos(true);
            }
        }
        else
        {
            if (GUILayout.Button("HideGizmos"))
            {
                ShowGizmos(false);
            }
        }
    }

    public void ShowGizmos(bool state)
    {
        PF_Area area = (PF_Area)target;
        area.showGizmos = state;
        foreach (PF_Region region in area.regions)
        {
            foreach (PF_Grid grid in region.grids)
            {
                foreach (PF_Node node in grid.nodes)
                {
                    node.showGizmo = state;
                }
            }
        }
    }
    
    void Bake()
    {
        if (bakeGrids)
        {            
            PF_Area area = (PF_Area)target;

            area.regions[currentRegion].Bake();
            currentRegion++;            
            if (currentRegion >= area.regions.Count)
            {
                for (int i = 0; i < area.regions.Count; i++)
                {
                    if (area.regions[i].grids.Count == 0)
                    {
                        PF_Region region= area.regions[i];
                        area.regions.Remove(region);
                        DestroyImmediate(region.gameObject);
                    }
                }

                Debug.Log("Grids baked successfully");
                bakeGrids = false;
            }
        }
    }
}
#endif
