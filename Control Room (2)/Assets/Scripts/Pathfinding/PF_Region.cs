﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PF_Region : MonoBehaviour {

    public PF_Area area;
    public List<PF_Grid> grids = new List<PF_Grid>();
    public Vector3 regionSize;
    public Vector3 gridSize;
    public float gridSpacing;


    public PF_Node GetNode(Vector3 position, Vector3 nearPosition)
    {
        float sizeX = regionSize.x * (gridSize.x );
        float sizeY = regionSize.y * (gridSize.y );
        if (position.x > transform.position.x + sizeX / 2 || position.x < transform.position.x - sizeX / 2 || position.z > transform.position.z + sizeY || position.z < transform.position.z - sizeY)
        {            
            return null;
        }        

        foreach (PF_Grid grid in grids)
        {
            PF_Node node = grid.GetNode(position, nearPosition);
            if (node != null)
                return node;
        }
        return null;
    }

    public void Bake()
    {
        List<PF_Grid> clearGrids = new List<PF_Grid>();
        foreach(PF_Grid grid in grids)
        {
            grid.Bake();
            if (grid.nodes.Count == 0)
                clearGrids.Add(grid);
        }        
        for (int i = 0; i < clearGrids.Count; i++)
        {
            PF_Grid grid = clearGrids[i];
            grids.Remove(grid);
            DestroyImmediate(grid.gameObject);
        }
    }

    public void GenerateGrid(Vector3 regionSize, Vector3 gridSize, float gridSpacing)
    {
        this.regionSize = regionSize;
        this.gridSize = gridSize;
        this.gridSpacing = gridSpacing;

        for (int i = 0; i < grids.Count; i++)
        {
            DestroyImmediate(grids[i].gameObject);
        }
        grids = new List<PF_Grid>();
        
        float xOffset = ((regionSize.x - 1) / 2) * gridSize.x;
        float yOffset = ((regionSize.y - 1) / 2) * gridSize.y;

        for (float x = -xOffset; x <= xOffset; x += gridSize.x)
        {
            for (float y = -yOffset; y <= yOffset; y += gridSize.y)
            {
                GameObject gridGO = new GameObject("Grid");
                gridGO.transform.position = new Vector3(x, 0, y) + transform.position;
                gridGO.transform.SetParent(transform);
                PF_Grid grid = gridGO.AddComponent<PF_Grid>();
                grid.GenerateGrid(gridSize, gridSpacing);
                grid.region = this;
                grids.Add(grid);

            }
        }
    }
}
