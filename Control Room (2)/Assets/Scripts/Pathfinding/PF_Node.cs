﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PF_Node : MonoBehaviour
{
    public PF_Grid grid;    
    public PF_Node parent;
    public List<PF_Node> neighbours = new List<PF_Node>();
    public bool unwalkable;
    public bool blocked;
    public float spacing;
    public bool showGizmo;

    public float avoidanceWeight;
    public float gCost;
    public float hCost;
    public float fCost { get { return hCost + gCost; } }

    public void Reset()
    {
        unwalkable = false;
        blocked = false;
        avoidanceWeight = 0;
        neighbours = new List<PF_Node>();
    }
    
    private void OnDrawGizmosSelected()
    {
        if (!showGizmo) return;
        if (unwalkable) Gizmos.color = Color.cyan;
        else if (blocked) Gizmos.color = Color.magenta;        
        else if (avoidanceWeight > 0) Gizmos.color = Color.red * (avoidanceWeight/50);        
        else Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, spacing/2);
    }
}
